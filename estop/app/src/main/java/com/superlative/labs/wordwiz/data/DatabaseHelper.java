package com.superlative.labs.wordwiz.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import com.superlative.labs.wordwiz.data.model.Category;
import com.superlative.labs.wordwiz.data.model.Level;
import com.superlative.labs.wordwiz.data.model.LevelChallenge;
import com.superlative.labs.wordwiz.data.model.LevelChallengeModifier;
import com.superlative.labs.wordwiz.data.model.PowerUp;
import com.superlative.labs.wordwiz.data.model.Stage;
import com.superlative.labs.wordwiz.data.model.Theme;
import com.superlative.labs.wordwiz.data.model.Word;
import com.superlative.labs.wordwiz.data.repositories.PowerUpRepository;
import com.superlative.labs.wordwiz.engine.Keys;

/**
 * Created by Catherine on 2/25/2016.
 */
public class DatabaseHelper extends RealDatabaseHelper{

    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/com.superlative.labs.wordwiz/databases/";

    private static String DB_NAME = "estop.db";

    private SQLiteDatabase myDataBase;

    private final Context myContext;

    private RuntimeExceptionDao<Theme, Integer> themeRuntimeDao;
    private RuntimeExceptionDao<Category, Integer> categoryRuntimeDao;
    private RuntimeExceptionDao<Word, Integer> wordRuntimeDao;

    private RuntimeExceptionDao<Stage, Integer> stageRuntimeDao;
    private RuntimeExceptionDao<Level, Integer> levelRuntimeDao;
    private RuntimeExceptionDao<LevelChallenge, Integer> levelChallengeRuntimeDao;
    private RuntimeExceptionDao<LevelChallengeModifier, Integer> levelChallengeModifierRuntimeDao;

    private RuntimeExceptionDao<PowerUp, Integer> powerUpRuntimeDao;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     * @param context
     */
    public DatabaseHelper(Context context) {

        super(context);
        this.myContext = context;
    }

    public Context getContext() {
        return this.myContext;
    }

    /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException{

        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }catch(SQLiteException e){

            //database does't exist yet.

        }

        if(checkDB != null){

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException{

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

    }

    @Override
    public synchronized void close() {

        if(myDataBase != null)
            myDataBase.close();

        super.close();

        categoryRuntimeDao = null;
        wordRuntimeDao = null;
        themeRuntimeDao = null;

        stageRuntimeDao = null;
        levelRuntimeDao = null;
        levelChallengeRuntimeDao = null;
        levelChallengeModifierRuntimeDao = null;

        powerUpRuntimeDao = null;

    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        Log.i(DatabaseHelper.class.getName(), "onCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        Log.i(DatabaseHelper.class.getName(), "onUpgrade");

//        PowerUpRepository repository = new PowerUpRepository(this);

//        repository.addPowerUp(Keys.Products.Skus.UnlockGame, Keys.Products.PackQuantity.One);
    }

    public RuntimeExceptionDao<Theme, Integer> getThemeDao() {
        if (themeRuntimeDao == null) {
            themeRuntimeDao = getRuntimeExceptionDao(Theme.class);
        }
        return themeRuntimeDao;
    }

    public RuntimeExceptionDao<Category, Integer> getCategoryDao() {
        if (categoryRuntimeDao == null) {
            categoryRuntimeDao = getRuntimeExceptionDao(Category.class);
        }
        return categoryRuntimeDao;
    }

    public RuntimeExceptionDao<Word, Integer> getWordDao() {
        if (wordRuntimeDao == null) {
            wordRuntimeDao = getRuntimeExceptionDao(Word.class);
        }
        return wordRuntimeDao;
    }

    public RuntimeExceptionDao<Stage, Integer> getStageDao() {
        if (stageRuntimeDao == null) {
            stageRuntimeDao = getRuntimeExceptionDao(Stage.class);
        }
        return stageRuntimeDao;
    }

    public RuntimeExceptionDao<Level, Integer> getLevelDao() {
        if (levelRuntimeDao == null) {
            levelRuntimeDao = getRuntimeExceptionDao(Level.class);
        }
        return levelRuntimeDao;
    }

    public RuntimeExceptionDao<LevelChallenge, Integer> getLevelChallengeDao() {
        if (levelChallengeRuntimeDao == null) {
            levelChallengeRuntimeDao = getRuntimeExceptionDao(LevelChallenge.class);
        }
        return levelChallengeRuntimeDao;
    }

    public RuntimeExceptionDao<LevelChallengeModifier, Integer> getLevelChallengeModifierDao() {
        if (levelChallengeModifierRuntimeDao == null) {
            levelChallengeModifierRuntimeDao = getRuntimeExceptionDao(LevelChallengeModifier.class);
        }
        return levelChallengeModifierRuntimeDao;
    }

    public RuntimeExceptionDao<PowerUp, Integer> getPowerUpDao() {
        if (powerUpRuntimeDao == null) {
            powerUpRuntimeDao = getRuntimeExceptionDao(PowerUp.class);
        }
        return powerUpRuntimeDao;
    }
}
