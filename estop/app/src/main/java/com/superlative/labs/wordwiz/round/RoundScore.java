package com.superlative.labs.wordwiz.round;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.superlative.labs.wordwiz.modifiers.IChallengeModifier;

public class RoundScore
{
    private int totalScore;
    private int timeScore;

    private LinkedHashMap<Integer, ChallengeScore> scoresByChallengeId;
    private boolean levelCompleted;

    public RoundScore(List<Integer> challengeIds) {

        this.totalScore = 0;
        this.timeScore = 0;

        this.scoresByChallengeId = new LinkedHashMap<>();

        for (Integer challengeId : challengeIds) {
            this.scoresByChallengeId.put(challengeId, new ChallengeScore());
        }
    }

    public int getTotalScore() {
        return totalScore;
    }

    public int getTimeScore() {
        return timeScore;
    }

    public boolean getLevelCompleted() {
        return levelCompleted;
    }

    public void setLevelCompleted(boolean levelCompleted) {
        this.levelCompleted = levelCompleted;
    }

    public LinkedHashMap<Integer, ChallengeScore> getScoresByChallengeId() {
        return scoresByChallengeId;
    }

    public void setTimeScore(int timeScore) {

        this.timeScore = timeScore;

        this.calculateTotalScore();
    }

    public void setZeroChallengeScore(Integer challengeId) {
        this.scoresByChallengeId.get(challengeId).setScore(0);
    }

    public void setChallengeSuccessScore(Integer challengeId, int challengeSuccessScore) {

        this.scoresByChallengeId.get(challengeId).setScore(challengeSuccessScore);

        this.calculateTotalScore();
    }

    public List<IChallengeModifier> getModifiersForChallengeId(Integer challengeId) {

        List<IChallengeModifier> challengeModifiers = new ArrayList<>();

        for (ModifierScore modifierScore :
                this.scoresByChallengeId.get(challengeId).getScoresByModifierId().values()) {
            challengeModifiers.add(modifierScore.getChallengeModifier());
        }

        return challengeModifiers;
    }

    public void addModifier(Integer challengeId, IChallengeModifier modifier) {

        ModifierScore modifierScore = new ModifierScore(0, modifier);

        this.scoresByChallengeId.get(challengeId).getScoresByModifierId().put(modifier.getId(), modifierScore);
    }

    public void setModifierScore(Integer challengeId, IChallengeModifier modifier, int scoreFromModifier) {

        ModifierScore modifierScore = this.scoresByChallengeId.get(challengeId).getScoresByModifierId().get(modifier.getId());

        modifierScore.setScore(scoreFromModifier);

        //ModifierScore modifierScore = new ModifierScore(scoreFromModifier, modifier);

        //this.scoresByChallengeId.get(challengeId).getScoresByModifierId().put(modifier.getId(), modifierScore);

        this.calculateTotalScore();
    }

    public void setZeroModifierScore(Integer challengeId, IChallengeModifier modifier) {

        ModifierScore modifierScore = this.scoresByChallengeId.get(challengeId).getScoresByModifierId().get(modifier.getId());

        modifierScore.setScore(0);

        //ModifierScore modifierScore = new ModifierScore(0, modifier);

        //this.scoresByChallengeId.get(challengeId).getScoresByModifierId().put(modifier.getId(), modifierScore);
    }

    private void calculateTotalScore() {

        this.totalScore = 0;

        this.totalScore += this.timeScore;

        for (Map.Entry<Integer, ChallengeScore> entry : scoresByChallengeId.entrySet()) {

            ChallengeScore challengeScore = entry.getValue();

            this.totalScore += challengeScore.getScore();

            for (Map.Entry<Integer, ModifierScore> scoreForModifierId : challengeScore.getScoresByModifierId().entrySet()) {

                Integer modifierScore = scoreForModifierId.getValue().getScore();

                this.totalScore += modifierScore;
            }
        }
    }

    public void setPlayedWordForChallengeId(String word, Integer challengeId) {
        this.scoresByChallengeId.get(challengeId).setPlayedWord(word);
    }

    public void setWordExists(Integer challengeId, boolean wordExists) {
        this.scoresByChallengeId.get(challengeId).setWordExists(wordExists);
    }
}