package com.superlative.labs.wordwiz.engine;

import android.os.AsyncTask;

public class CountdownTask extends AsyncTask<Void, Void, Void>
{
    public interface ICountdownProgressObserver
    {
        void notifyCountdownInit();
        void notifySecondPassed(int secondsLeft);
        void notifyCountdownOver();
    }
    
    ICountdownProgressObserver observer;
    
    int secondsLeft;
    final int ONE_SECOND = 1000;
    final int ZERO = 0;

    //boolean paused = false;

    public CountdownTask(ICountdownProgressObserver observer)
    {
        this.observer = observer;
        this.secondsLeft = 0;

        this.observer.notifyCountdownInit();
    }
    
    public CountdownTask(ICountdownProgressObserver observer, int countdownSeconds)
    {
        this.observer = observer;
        this.secondsLeft = countdownSeconds;
        
        this.observer.notifyCountdownInit();
    }

    public void increaseSecondsLeft(int secondsToAdd) {
        this.secondsLeft += secondsToAdd;
    }

//    public void pause() {
//        this.paused = true;
//    }
//
//    public void resume() {
//        this.paused = false;
//    }
    
    protected Void doInBackground(Void... nothing)
    {
        while (!this.isCancelled()/* && !this.paused*/)
        {
            try
            { 
                Thread.sleep(ONE_SECOND);
            }
            catch (InterruptedException e) 
            {
                continue; //>_<
            }
            
            this.secondsLeft--;
            
            this.publishProgress();
            
            if (this.secondsLeft <= ZERO) {
                break;
            }
        }
        
        return null;
    }
    
    protected void onProgressUpdate(Void... nothing)
    {
        this.observer.notifySecondPassed(this.secondsLeft);
    }
    
    protected void onPostExecute(Void result)
    {
        this.observer.notifyCountdownOver();
    }
}