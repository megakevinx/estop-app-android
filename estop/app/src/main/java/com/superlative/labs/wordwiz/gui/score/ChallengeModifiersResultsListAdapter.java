package com.superlative.labs.wordwiz.gui.score;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.LinkedHashMap;

import com.superlative.labs.wordwiz.R;

import com.superlative.labs.wordwiz.round.ModifierScore;

/**
 * Created by kevin on 11/9/2015.
 */
public class ChallengeModifiersResultsListAdapter extends BaseAdapter implements ListAdapter {

    private final Context context;
    private final LinkedHashMap<Integer, ModifierScore> scoresByModifierId;

    public ChallengeModifiersResultsListAdapter(
            Context context,
            LinkedHashMap<Integer, ModifierScore> scoresByModifierId) {

        this.context = context;
        this.scoresByModifierId = scoresByModifierId;
    }

    @Override
    public int getCount() {
        return this.scoresByModifierId.size();
    }

    @Override
    public Object getItem(int position) {
        return (ModifierScore) this.scoresByModifierId.values().toArray()[position];
    }

    @Override
    public long getItemId(int position) {
        return (Integer) this.scoresByModifierId.keySet().toArray()[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ModifierScore modifierScore = (ModifierScore) this.getItem(position);
        int layoutToLoad;

        if (modifierScore.getScore() > 0) {
            layoutToLoad = R.layout.list_item_modifier_success;
        }
        else {
            layoutToLoad = R.layout.list_item_modifier_failure;
        }

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutToLoad, parent, false);
        }

        TextView textViewModifier = (TextView) convertView.findViewById(R.id.textView_Modifier);
        TextView textViewScore = (TextView) convertView.findViewById(R.id.textView_Score);

        textViewModifier.setText(modifierScore.getChallengeModifier().toString());
        textViewScore.setText(Integer.toString(modifierScore.getScore()));

        return convertView;
    }
}
