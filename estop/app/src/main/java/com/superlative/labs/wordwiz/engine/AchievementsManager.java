package com.superlative.labs.wordwiz.engine;

import java.util.ArrayList;

import com.superlative.labs.wordwiz.data.repositories.ILevelRepository;
import com.superlative.labs.wordwiz.utils.achievements.IAchievementApiClient;

/**
 * Created by kevin on 2/3/2016.
 */
public class AchievementsManager {

    public interface IAchievementsManagerHost {
        boolean getLevelCompleted();
        ArrayList<String> getPlayedWords();
        int getSecondsLeft();
    }

    // TODO: Define an interface that the ScoreActivity can
    // implement for communicating with this class.
    private IAchievementsManagerHost host;
    private IAchievementApiClient apiClient;
    private final ILevelRepository levelRepository;

    public AchievementsManager(
            IAchievementsManagerHost host,
            IAchievementApiClient achievementApiClient,
            ILevelRepository levelRepository) {

        this.host = host;
        this.apiClient = achievementApiClient;
        this.levelRepository = levelRepository;
    }

    public void checkAchievements() {
        this.checkLevelsCompletedAchievements();
        this.checkScoreAchievements();

        this.checkOneTimeConditionAchievements();
    }

    private void checkOneTimeConditionAchievements() {

        if (this.host.getLevelCompleted()) {

            this.checkWordCharacterNumberAchievement(10, Keys.Achievements.TenCharacterWord);
            this.checkWordCharacterNumberAchievement(15, Keys.Achievements.FifteenCharacterWord);

            this.checkAllWordsCharacterNumberAchievement(3, Keys.Achievements.AllWordsThreeCharacters);
            this.checkAllWordsCharacterNumberAchievement(5, Keys.Achievements.AllWordsFiveCharacters);
            this.checkAllWordsCharacterNumberAchievement(10, Keys.Achievements.AllWordsTenCharacters);

            this.checkWordPlayedAchievement(Keys.Achievements.Words.Candle, Keys.Achievements.PlayedWordCandle);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Umbrella, Keys.Achievements.PlayedWordUmbrella);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Fire, Keys.Achievements.PlayedWordFire);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Short, Keys.Achievements.PlayedWordShort);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Stamp, Keys.Achievements.PlayedWordStamp);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Secret, Keys.Achievements.PlayedWordSecret);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Clock, Keys.Achievements.PlayedWordClock);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Cold, Keys.Achievements.PlayedWordCold);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Breath, Keys.Achievements.PlayedWordBreath);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Silence, Keys.Achievements.PlayedWordSilence);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Match, Keys.Achievements.PlayedWordMatch);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Palm, Keys.Achievements.PlayedWordPalm);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Needle, Keys.Achievements.PlayedWordNeedle);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.President, Keys.Achievements.PlayedWordPresident);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Stars, Keys.Achievements.PlayedWordStars);
            this.checkWordPlayedAchievement(Keys.Achievements.Words.Promise, Keys.Achievements.PlayedWordPromise);

            this.checkLevelCompletedWithLessThanFiveSecondsLeft();
            this.checkLevelCompletedWithMoreThanFiftySecondsLeft();
        }
    }

    private void checkWordPlayedAchievement(String wordToCheck, String achievementCode) {

        ArrayList<String> playedWords = this.host.getPlayedWords();
        boolean wordPlayed = false;

        for (String word : playedWords) {
            if (word.equals(wordToCheck)) {
                wordPlayed = true;
                break;
            }
        }

        if (wordPlayed)
            this.apiClient.unlock(achievementCode);
    }

    private void checkAllWordsCharacterNumberAchievement(int characterCount, String achievementCode) {

        ArrayList<String> playedWords = this.host.getPlayedWords();
        boolean allWordsAreRequiredLength = true;

        for (String word : playedWords) {
            if (word.length() != characterCount) {
                allWordsAreRequiredLength = false;
                break;
            }
        }

        if (allWordsAreRequiredLength)
            this.apiClient.unlock(achievementCode);
    }

    private void checkWordCharacterNumberAchievement(int characterCount, String achievementCode) {

        ArrayList<String> playedWords = this.host.getPlayedWords();
        boolean hasWordCharacterCount = false;

        for (String word : playedWords) {
            if (word.length() >= characterCount) {
                hasWordCharacterCount = true;
                break;
            }
        }

        if (hasWordCharacterCount)
            this.apiClient.unlock(achievementCode);
    }

    private void checkLevelCompletedWithLessThanFiveSecondsLeft() {

        int secondsLeft = this.host.getSecondsLeft();

        if (secondsLeft < 5)
            this.apiClient.unlock(Keys.Achievements.LevelCompletedWithLessThanFiveSecondsLeft);
    }

    private void checkLevelCompletedWithMoreThanFiftySecondsLeft() {

        int secondsLeft = this.host.getSecondsLeft();

        if (secondsLeft > 50)
            this.apiClient.unlock(Keys.Achievements.LevelCompletedWithMoreThanFiftySecondsLeft);
    }

    private void checkScoreAchievements() {
        int totalScore = this.levelRepository.getTotalScore();

        if (totalScore >= 140000)
            this.apiClient.unlock(Keys.Achievements.WordGuru);
        else if (totalScore >= 130000)
            this.apiClient.unlock(Keys.Achievements.LanguageDoctor);
        else if (totalScore >= 120000)
            this.apiClient.unlock(Keys.Achievements.Professor);
        else if (totalScore >= 110000)
            this.apiClient.unlock(Keys.Achievements.Teacher);
        else if (totalScore >= 100000)
            this.apiClient.unlock(Keys.Achievements.Lecturer);
        else if (totalScore >= 90000)
            this.apiClient.unlock(Keys.Achievements.TeachingAssistant);
        else if (totalScore >= 80000)
            this.apiClient.unlock(Keys.Achievements.Litterateur);
        else if (totalScore >= 70000)
            this.apiClient.unlock(Keys.Achievements.Author);
        else if (totalScore >= 60000)
            this.apiClient.unlock(Keys.Achievements.Novelist);
        else if (totalScore >= 50000)
            this.apiClient.unlock(Keys.Achievements.Writer);
        else if (totalScore >= 40000)
            this.apiClient.unlock(Keys.Achievements.Editor);
        else if (totalScore >= 30000)
            this.apiClient.unlock(Keys.Achievements.Journalist);
        else if (totalScore >= 20000)
            this.apiClient.unlock(Keys.Achievements.Blogger);
        else if (totalScore >= 10000)
            this.apiClient.unlock(Keys.Achievements.ForumUser);
        else if (totalScore >= 5000)
            this.apiClient.unlock(Keys.Achievements.Commenter);
    }

    private void checkLevelsCompletedAchievements() {

        int numberOfLevelsCompleted = this.levelRepository.getCompletedLevelsCount();

        if (numberOfLevelsCompleted == 1)
            this.apiClient.unlock(Keys.Achievements.Beginner);
        else if (numberOfLevelsCompleted == 10) 
            this.apiClient.unlock(Keys.Achievements.Novice);
        else if (numberOfLevelsCompleted == 50) 
            this.apiClient.unlock(Keys.Achievements.Adept);
        else if (numberOfLevelsCompleted == 100) 
            this.apiClient.unlock(Keys.Achievements.Intermediate);
        else if (numberOfLevelsCompleted == 200) 
            this.apiClient.unlock(Keys.Achievements.Advanced);
        else if (numberOfLevelsCompleted == 300) 
            this.apiClient.unlock(Keys.Achievements.Master);
        else if (numberOfLevelsCompleted == 400) 
            this.apiClient.unlock(Keys.Achievements.Legend);
    }
}
