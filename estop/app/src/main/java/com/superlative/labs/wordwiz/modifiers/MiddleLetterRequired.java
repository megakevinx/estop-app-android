package com.superlative.labs.wordwiz.modifiers;

import android.util.Log;

/**
 * Created by Catherine on 11/25/2015.
 */
public class MiddleLetterRequired  extends BaseChallengeModifier  {

    private String letter;

    public MiddleLetterRequired(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        String middleLetter = Character.toString(word.charAt(word.length() / 2));
        Log.d("MiddleLetterRequired", "Middle letter: " + middleLetter);
        Log.d("MiddleLetterRequired", "is odd: " + (word.length()%2 > 0) );

        if(middleLetter != null){
            return word.length()%2 > 0 && middleLetter.toLowerCase().equals(letter.toLowerCase());
        } else {
            return false;
        }


    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Middle letter is";
    }

    @Override
    public String toString() {
        return "Middle: " + this.letter.toUpperCase();
    }
}
