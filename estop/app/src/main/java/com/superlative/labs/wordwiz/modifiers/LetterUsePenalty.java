package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by kevin on 8/18/2015.
 */
public class LetterUsePenalty extends BaseChallengeModifier {

    private String letter;

    public LetterUsePenalty(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return -100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        return word.toLowerCase().contains(this.letter.toLowerCase());
    }

    @Override
    public boolean isMandatory() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Letter Use Penalty";
    }

    @Override
    public String toString() {
        return "Avoid: " + this.letter.toUpperCase();
    }
}
