package com.superlative.labs.wordwiz.modifiers;

import com.superlative.labs.wordwiz.data.model.Category;
import com.superlative.labs.wordwiz.data.repositories.WordRepository;

/**
 * Created by kevin on 8/18/2015.
 */
public class CategoryRequired extends BaseChallengeModifier {

    private Category category;
    private WordRepository wordRepository;

    public CategoryRequired(int id, Category category, WordRepository wordRepository) {
        this.id = id;
        this.category = category;
        this.wordRepository = wordRepository;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        return wordRepository.wordIsInCategory(word.toLowerCase(), this.category);
    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Required Category";
    }
}
