package com.superlative.labs.wordwiz.data.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by kevin on 8/29/2015.
 */

@DatabaseTable
public class Stage {
    public final static String ID = "id";
    @DatabaseField(id = true)
    int id;

    @DatabaseField(canBeNull = false)
    int sequence;

    @DatabaseField
    String name;

    public final static String IS_LOCKED = "is_locked";
    @DatabaseField(canBeNull = false, columnName = IS_LOCKED)
    boolean isLocked;

    @ForeignCollectionField
    ForeignCollection<Level> levels;

    Stage() { }

    public Stage(int id, int sequence, String name, boolean isLocked)
    {
        this.id = id;
        this.sequence = sequence;
        this.name = name;
        this.isLocked = isLocked;
    }

    public int getId() {
        return id;
    }

    public int getSequence() {
        return sequence;
    }

    public String getName() {
        return name;
    }

    public ForeignCollection<Level> getLevels() {
        return levels;
    }

    public boolean getIsLocked() {
        return isLocked;
    }
}
