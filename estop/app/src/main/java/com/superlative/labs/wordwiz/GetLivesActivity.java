package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class GetLivesActivity extends BaseGameActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_lives);
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    public void button_PurchaseLifePack_OnClick(View sender) {
        this.startStoreActivity();
    }

    private void startStoreActivity() {
        Intent intent = new Intent(this, StoreActivity.class);

        intent.putExtra(Keys.IntentParameters.AutoBuy, true);
        intent.putExtra(Keys.IntentParameters.AutoBuySku, Keys.Products.Skus.Life);

        this.startActivity(intent);
    }
}
