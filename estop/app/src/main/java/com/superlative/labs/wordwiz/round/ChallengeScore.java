package com.superlative.labs.wordwiz.round;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by kevin on 9/26/2015.
 */
public class ChallengeScore implements Parcelable {

    private int score;
    private String playedWord;
    private boolean wordExists;
    private LinkedHashMap<Integer, ModifierScore> scoresByModifierId;

    public ChallengeScore() {

        this.score = 0;
        this.playedWord = "";
        this.scoresByModifierId = new LinkedHashMap<>();
    }

    public LinkedHashMap<Integer, ModifierScore> getScoresByModifierId() {
        return scoresByModifierId;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public void setPlayedWord(String playedWord) {
        this.playedWord = playedWord;
    }

    public String getPlayedWord() {
        return this.playedWord;
    }

    public void setWordExists(boolean wordExists) {
        this.wordExists = wordExists;
    }

    // 99.9% of the time you can just ignore this
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    public void writeToParcel(Parcel out, int flags) {

        out.writeInt(this.score);
        out.writeString(this.playedWord);
        out.writeByte((byte) (this.wordExists ? 1 : 0));

        int[] modifierIdsToParcel = new int[this.scoresByModifierId.size()];
        int[] modifierScoresToParcel = new int[this.scoresByModifierId.size()];

        int index = 0;

        for(Map.Entry<Integer, ModifierScore> entry : this.scoresByModifierId.entrySet()) {
            Integer modifierId = entry.getKey();
            Integer score = entry.getValue().getScore();

            modifierIdsToParcel[index] = modifierId;
            modifierScoresToParcel[index] = score;

            index++;
        }

        out.writeIntArray(modifierIdsToParcel);
        out.writeIntArray(modifierScoresToParcel);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<ChallengeScore> CREATOR = new Parcelable.Creator<ChallengeScore>() {
        public ChallengeScore createFromParcel(Parcel in) {
            return new ChallengeScore(in);
        }

        public ChallengeScore[] newArray(int size) {
            return new ChallengeScore[size];
        }
    };

    // constructor that takes a Parcel and gives you an object populated with it's values
    private ChallengeScore(Parcel in) {

        this.scoresByModifierId = new LinkedHashMap<>();

        this.score = in.readInt();
        this.playedWord = in.readString();
        this.wordExists = in.readByte() != 0;

        int[] modifierIdsToLoad = in.createIntArray();
        int[] modifierScoresToLoad = in.createIntArray();

        for (int i = 0; i < modifierIdsToLoad.length; i++) {
            // This is a hack. Sending null instead of a IChallengeModifier. There's no easy way to reconstruct an IChallengeModifier object from the parcel.
            this.scoresByModifierId.put(modifierIdsToLoad[i], new ModifierScore(modifierScoresToLoad[i], null));
        }
    }

    public boolean getWordExists() {
        return wordExists;
    }
}
