package com.superlative.labs.wordwiz.utils.achievements;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;

/**
 * Created by kevin on 2/8/2016.
 */
public class AchievementApiClient implements IAchievementApiClient {

    GoogleApiClient googleApiClient;

    public AchievementApiClient(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    public void unlock(String achievementCode) {
        Games.Achievements.unlock(this.googleApiClient, achievementCode);
    }
}
