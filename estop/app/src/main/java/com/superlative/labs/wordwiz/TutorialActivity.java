package com.superlative.labs.wordwiz;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;

import com.superlative.labs.wordwiz.gui.tutorial.TutorialPagerAdapter;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class TutorialActivity extends BaseGameActivity {

    private ViewPager tutorialViewPager;

    private PagerAdapter tutorialPagerAdapter;

    private int[] progressItemIds = new int[] {
            R.id.imageView_TutorialProgress1,
            R.id.imageView_TutorialProgress2,
            R.id.imageView_TutorialProgress3,
            R.id.imageView_TutorialProgress4,
            R.id.imageView_TutorialProgress5,
            R.id.imageView_TutorialProgress6,
            R.id.imageView_TutorialProgress7,
            R.id.imageView_TutorialProgress8,
            R.id.imageView_TutorialProgress9,
            R.id.imageView_TutorialProgress10
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        tutorialViewPager = (ViewPager) findViewById(R.id.ViewPager_TutorialPages);
        tutorialPagerAdapter = new TutorialPagerAdapter(getSupportFragmentManager());
        tutorialViewPager.setAdapter(tutorialPagerAdapter);

        tutorialViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                notifyViewPagerPageScrolled(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void notifyViewPagerPageScrolled(int pageNumber){

        for (int itemId : progressItemIds) {
            ImageView imageViewNotCurrentProgressItem = (ImageView) findViewById(itemId);
            imageViewNotCurrentProgressItem.setBackgroundResource(R.drawable.tutorial_progress_item_normal);
        }

        int currentProgressItemId = progressItemIds[pageNumber];

        ImageView imageViewCurrentProgressItem = (ImageView) findViewById(currentProgressItemId);
        imageViewCurrentProgressItem.setBackgroundResource(R.drawable.tutorial_progress_item_current);
    }

    @Override
    public void onBackPressed() {
        if (tutorialViewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            tutorialViewPager.setCurrentItem(tutorialViewPager.getCurrentItem() - 1);
        }
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }
}
