package com.superlative.labs.wordwiz.data.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by kevin on 8/29/2015.
 */

@DatabaseTable
public class Level {

    public final static String ID = "id";
    @DatabaseField(id = true, columnName = ID)
    int id;

    @DatabaseField(canBeNull = false)
    int sequence;

    public final static String STAGE_ID = "stage_id";
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    Stage stage;

    /* The name of the level? */
    @DatabaseField
    String name;

    @DatabaseField(canBeNull = false)
    int seconds;

    @ForeignCollectionField
    ForeignCollection<LevelChallenge> levelChallenges;

    public static final String SCORE = "score";
    @DatabaseField()
    int score;

    public static final String IS_COMPLETED = "is_completed";
    @DatabaseField(canBeNull = false, columnName = IS_COMPLETED)
    boolean isCompleted;

    public final static String IS_LOCKED = "is_locked";
    @DatabaseField(canBeNull = false, columnName = IS_LOCKED)
    boolean isLocked;

    Level() { }

    public Level(int id, int sequence, Stage stage, String name, int seconds, int score, boolean isCompleted, boolean isLocked) {
        this.id = id;
        this.sequence = sequence;
        this.stage = stage;
        this.name = name;
        this.seconds = seconds;
        this.score = score;
        this.isCompleted = isCompleted;
        this.isLocked = isLocked;
    }

    public int getId() {
        return id;
    }

    public int getSequence() {
        return sequence;
    }

    public Stage getStage() {
        return stage;
    }

    public String getName() {
        return name;
    }

    public int getSeconds() {
        return seconds;
    }

    public Collection<LevelChallenge> getLevelChallenges() {
        return levelChallenges;
    }

    public boolean getIsCompleted() {
        return isCompleted;
    }

    public int getScore() {
        return score;
    }

    public boolean getIsLocked() {
        return isLocked;
    }
}
