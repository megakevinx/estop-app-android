package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by kevin on 10/1/2015.
 */
public abstract class BaseChallengeModifier implements IChallengeModifier {

    protected int id;

    @Override
    public int getId() {
        return this.id;
    }
}
