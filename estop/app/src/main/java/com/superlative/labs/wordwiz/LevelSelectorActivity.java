package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Level;
import com.superlative.labs.wordwiz.data.repositories.LevelRepository;
import com.superlative.labs.wordwiz.data.repositories.PowerUpRepository;
import com.superlative.labs.wordwiz.gui.level.LevelsListAdapter;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

import java.util.List;

public class LevelSelectorActivity extends BaseGameActivity {

    private String TAG = "LevelSelectorActivity";
    private LevelRepository levelRepository;
    private PowerUpRepository powerUpRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_selector);

        Intent intent = getIntent();

        int selectedStageId = intent.getIntExtra(Keys.IntentParameters.StageId, 1);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        this.levelRepository = new LevelRepository(databaseHelper);
        this.powerUpRepository = new PowerUpRepository(databaseHelper);


        ListView levelsListView = (ListView)findViewById(R.id.listView_Levels);

        List<Level> levelsInStage = this.levelRepository.getByStageId(selectedStageId);
        
        final LevelsListAdapter levelsAdapter = new LevelsListAdapter(this, levelsInStage);

        levelsListView.setAdapter(levelsAdapter);

        levelsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "Position: " + Integer.toString(position) + " - level id: " + Long.toString(id));

                Level clickedLevel = (Level) levelsAdapter.getItem(position);

                if (!clickedLevel.getIsLocked()) {
                    startGame((int) id);
                }
            }
        });

        int levelPosition = 0;

        for (Level level :  levelsInStage) {
            if (level.getIsCompleted()) {
                levelPosition++;
            }
        }

        levelsListView.setSelection(levelPosition);
    }

    private void startGame(int levelId) {
        int currentLives = this.powerUpRepository.getCurrentLives();

            if (currentLives > 0) {
                this.startGameActivity(levelId);
            }
            else {
                this.startGetLivesActivity();
            }
    }

    private void startGetLivesActivity() {
        Intent intent = new Intent(this, GetLivesActivity.class);
        this.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, StageSelectorActivity.class);
        this.startActivity(intent);
    }

    private void startGameActivity(int selectedLevelId) {

        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(Keys.IntentParameters.GameLoadMode, Keys.IntentParameters.GameLoadModes.LevelSelected);
        intent.putExtra(Keys.IntentParameters.LevelId, selectedLevelId);

        this.startActivity(intent);
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }
}
