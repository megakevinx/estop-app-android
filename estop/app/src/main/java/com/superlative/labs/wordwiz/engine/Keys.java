package com.superlative.labs.wordwiz.engine;

public class Keys 
{
    public static String PackageName = "com.superlative.labs.wordwiz";

    public static String LEADERBOARD_ID = "CgkIhq284ZYGEAIQAQ";

    public static String PublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhl5wGIOub8vgp3GmB6/cjZxOkXQz403MYtfDvVafdhPYC8tFJ+ShxnMhi9bVnzaCvozXxM7gQDj2Y2Lk60wfjEBfqEQGtjGEu+ZJ3KAkZUuAubMz5wJ71+4usKC5gEHK7ny4AYLDMfO71gsdst2ZB7HXIQoOIAhclo8R0OzFjyi1raMNjApbzMxk+VN5FftnuoTbmUb/c6mJVGXLsE8Qlse5fRbo7ROb0kvn/lgigWGpeg3a9/E6SGUB+x4maSO77dfAjnuohjRCvcINIysavdsUDg1/qSiVJp1Kz8g0jp6pjeDCKywNwXAxZNf0+BcT33ay7ju30sY5rhOFVm0UjwIDAQAB";

    public interface IntentParameters {
        String LevelId = "lostigueres.stop.Keys.IntentParameters.LevelId";
        String StageId = "lostigueres.stop.Keys.IntentParameters.StageId";
        String GameLoadMode = "lostigueres.stop.Keys.IntentParameters.GameLoadMode";
        String FinishRoundData = "lostigueres.stop.Keys.IntentParameters.FinishRoundData";
        String ChallengeIndex = "lostigueres.stop.Keys.IntentParameters.ChallengeIndex";
        String ChallengeId = "lostigueres.stop.Keys.IntentParameters.ChallengeId";
        String ChallengeScore = "lostigueres.stop.Keys.IntentParameters.ChallengeScore";
        String AutoBuy = "lostigueres.stop.Keys.IntentParameters.AutoBuy";
        String AutoBuySku = "lostigueres.stop.Keys.IntentParameters.AutoBuySku";
        String HasTimePlus = "lostigueres.stop.Keys.IntentParameters.HasTimePlus";;

        interface GameLoadModes {
            String LevelSelected = "lostigueres.stop.Keys.IntentParameters.GameLoadModeModes.LevelSelected";
        }
    }

    public interface Products {

        interface Skus {
            String TimePlus = "tst001";
            String Life = "tst002";
            String RemoveAds = "tst003";
            String UnlockGame = "tst004";
        }

        interface PackQuantity {
            int Five = 5;
            int Ten = 10;
            int Twenty = 20;
            int Fifty = 50;
            int One = 1;
            int Forty = 40;
        }

        interface UseQuantity {
            int One = 1;
        }

        interface SecondsIncrease {
            int Ten = 10;
        }
    }

    public interface Preferences {
        String LastTimeLivesGranted = "lostigueres.stop.Keys.Preferences.LastTimeLivesGranted";
        String FirstTimeLivesGranted = "lostigueres.stop.Keys.Preferences.FirstTimeLivesGranted";
    }

    public interface Settings {
        int HoursToGrantLives = 1;
    }

    public interface Achievements {
        String Beginner = "CgkIhq284ZYGEAIQAg";
        String Novice  = "CgkIhq284ZYGEAIQAw";
        String Adept = "CgkIhq284ZYGEAIQBA";
        String Intermediate  = "CgkIhq284ZYGEAIQBQ";
        String Advanced = "CgkIhq284ZYGEAIQBg";
        String Master = "CgkIhq284ZYGEAIQCg";
        String Legend = "CgkIhq284ZYGEAIQCw";

        String Commenter = "CgkIhq284ZYGEAIQDA";
        String ForumUser  = "CgkIhq284ZYGEAIQDQ";
        String Blogger = "CgkIhq284ZYGEAIQDg";
        String Journalist  = "CgkIhq284ZYGEAIQDw";
        String Editor = "CgkIhq284ZYGEAIQEA";
        String Writer = "CgkIhq284ZYGEAIQEQ";
        String Novelist = "CgkIhq284ZYGEAIQEg";
        String Author = "CgkIhq284ZYGEAIQEw";
        String Litterateur = "CgkIhq284ZYGEAIQFA";
        String TeachingAssistant = "CgkIhq284ZYGEAIQFQ";
        String Lecturer = "CgkIhq284ZYGEAIQFg";
        String Teacher = "CgkIhq284ZYGEAIQFw";
        String Professor = "CgkIhq284ZYGEAIQGA";
        String LanguageDoctor = "CgkIhq284ZYGEAIQGQ";
        String WordGuru = "CgkIhq284ZYGEAIQGg";

        String TenCharacterWord = "CgkIhq284ZYGEAIQGw";
        String FifteenCharacterWord = "CgkIhq284ZYGEAIQHA";

        String AllWordsThreeCharacters = "CgkIhq284ZYGEAIQHQ";
        String AllWordsFiveCharacters = "CgkIhq284ZYGEAIQHg";
        String AllWordsTenCharacters = "CgkIhq284ZYGEAIQHw";

        String LevelCompletedWithLessThanFiveSecondsLeft = "CgkIhq284ZYGEAIQIA";
        String LevelCompletedWithMoreThanFiftySecondsLeft = "CgkIhq284ZYGEAIQIQ";

        interface Words {
            String Candle = "CANDLE";
            String Umbrella = "UMBRELLA";
            String Fire = "FIRE";
            String Short = "SHORT";
            String Stamp = "STAMP";
            String Secret = "SECRET";
            String Clock = "CLOCK";
            String Cold = "COLD";
            String Breath = "BREATH";
            String Silence = "SILENCE";
            String Match = "MATCH";
            String Palm = "PALM";
            String Needle = "NEEDLE";
            String President = "PRESIDENT";
            String Stars = "STARS";
            String Promise = "PROMISE";
        }

        String PlayedWordCandle = "CgkIhq284ZYGEAIQIg";
        String PlayedWordUmbrella = "CgkIhq284ZYGEAIQIw";
        String PlayedWordFire = "CgkIhq284ZYGEAIQJA";
        String PlayedWordShort = "CgkIhq284ZYGEAIQJQ";
        String PlayedWordStamp = "CgkIhq284ZYGEAIQJg";
        String PlayedWordSecret = "CgkIhq284ZYGEAIQJw";
        String PlayedWordClock = "CgkIhq284ZYGEAIQKA";
        String PlayedWordCold = "CgkIhq284ZYGEAIQKQ";
        String PlayedWordBreath = "CgkIhq284ZYGEAIQKg";
        String PlayedWordSilence = "CgkIhq284ZYGEAIQKw";
        String PlayedWordMatch = "CgkIhq284ZYGEAIQLA";
        String PlayedWordPalm = "CgkIhq284ZYGEAIQLQ";
        String PlayedWordNeedle = "CgkIhq284ZYGEAIQLg";
        String PlayedWordPresident = "CgkIhq284ZYGEAIQLw";
        String PlayedWordStars = "CgkIhq284ZYGEAIQMA";
        String PlayedWordPromise = "CgkIhq284ZYGEAIQMQ";
    }
}
