package com.superlative.labs.wordwiz.round;

import java.util.List;

import com.superlative.labs.wordwiz.data.model.Word;
import com.superlative.labs.wordwiz.modifiers.IChallengeModifier;

/**
 * Created by Catherine on 8/20/2015.
 */

public class RoundWord {

    private Word word;
    private List<IChallengeModifier> constraints;

    public RoundWord(Word word, List<IChallengeModifier> constraints){
        this.word = word;
        this.constraints = constraints;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public List<IChallengeModifier> getConstraints() {
        return constraints;
    }

    public void setConstraints(List<IChallengeModifier> constraints) {
        this.constraints = constraints;
    }
}
