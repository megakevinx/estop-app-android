package com.superlative.labs.wordwiz.gui.tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.superlative.labs.wordwiz.R;


/**
 * Created by Catherine on 3/7/2016.
 */
public class TutorialPage extends Fragment {

    public static final String ARG_IMAGE_RES_ID = "imageResourceId";
    public static final String ARG_DESCRIPTION = "description";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();
        int imageResourceId = args.getInt(ARG_IMAGE_RES_ID);
        String description = args.getString(ARG_DESCRIPTION);

        View rootView = inflater.inflate(R.layout.fragment_tutorial_page, container, false);

        ImageView imageViewPicture = (ImageView) rootView.findViewById(R.id.imageView_Picture);
        TextView textViewDescription = (TextView) rootView.findViewById(R.id.textView_Description);

        imageViewPicture.setImageResource(imageResourceId);
        textViewDescription.setText(description);

        return rootView;
    }
}
