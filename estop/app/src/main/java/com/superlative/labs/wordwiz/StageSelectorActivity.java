package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Stage;
import com.superlative.labs.wordwiz.data.repositories.StageRepository;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.gui.stage.StagesListAdapter;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class StageSelectorActivity extends BaseGameActivity {

    private String TAG = "StageSelectorActivity";
    private StageRepository stageRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage_selector);



        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        this.stageRepository = new StageRepository(databaseHelper);

        ListView stagesListView = (ListView)findViewById(R.id.listView_Stages);

        final StagesListAdapter stagesAdapter = new StagesListAdapter(this, this.stageRepository.getAllStages());

        stagesListView.setAdapter(stagesAdapter);

        stagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "Position: " + Integer.toString(position) + " - id: " + Long.toString(id));

                Stage clickedStage = (Stage) stagesAdapter.getItem(position);

                if (!clickedStage.getIsLocked()) {
                    startLevelSelectorActivity((int) id);
                }
            }
        });

        Stage currentStage = this.stageRepository.getCurrentStage();
        stagesListView.setSelection(currentStage.getId() - 1);
    }

    private void startLevelSelectorActivity(int selectedStageId) {

        Intent intent = new Intent(this, LevelSelectorActivity.class);
        intent.putExtra(Keys.IntentParameters.StageId, selectedStageId);

        this.startActivity(intent);
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, TitleActivity.class);
        this.startActivity(intent);
    }
}
