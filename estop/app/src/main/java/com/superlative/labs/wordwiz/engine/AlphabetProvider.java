package com.superlative.labs.wordwiz.engine;

import java.util.Random;

public class AlphabetProvider 
{
	String[] alphabet;
	Random randomProvider;
	
	final int A = 65;
	final int Z = 90;
	final int ALPHABET_LENGTH = 26;
	
	public AlphabetProvider()
	{
		this.generateAlphabet();
		this.randomProvider = new Random();		
	}
	
	private void generateAlphabet() 
	{
		this.alphabet = new String[ALPHABET_LENGTH];
		
		for(int l = A; l <= Z; l++)
		{
			alphabet[l - A] = Character.toString((char)l);
		}
	}
	
	public String getRandomLetter()
	{
		return this.alphabet[this.randomProvider.nextInt(ALPHABET_LENGTH)];
	}
}