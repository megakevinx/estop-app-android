package com.superlative.labs.wordwiz.data.repositories;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Theme;

/**
 * Created by Catherine on 7/12/2015.
 */
public class ThemeRepository {

    private RuntimeExceptionDao<Theme, Integer> dao;

    public ThemeRepository(DatabaseHelper helper) {
        this.dao = helper.getThemeDao();
    }

    public List<String> getAllThemeNames() {

        List<Theme> themeList =  this.dao.queryForAll();

        List<String> themeNames = new ArrayList<String>();

        for (Theme theme : themeList) {
            themeNames.add(theme.getName());
        }

        return themeNames;
    }

    public List<Theme> getAllThemes(){
        return this.dao.queryForAll();
    }

    public Theme getThemeByName(String name) {

        Theme theme = null;
        try {
            QueryBuilder<Theme, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq("name", name);

            theme = this.dao.queryForFirst(queryBuilder.prepare());

            return theme;
        }
        catch (SQLException ex) {

        }
        return theme;
    }

    public Theme getThemeById(int id) {
        return this.dao.queryForId(id);
    }

    public void saveTheme(Theme theme){
        this.dao.create(theme);
    }

}
