package com.superlative.labs.wordwiz.data.repositories;

import com.superlative.labs.wordwiz.data.model.Level;

/**
 * Created by kevin on 2/10/2016.
 */
public interface ILevelRepository {
    int getTotalScore();
    int getCompletedLevelsCount();
    void unlockNextLevel(Level level);
}
