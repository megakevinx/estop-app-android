package com.superlative.labs.wordwiz.gui.tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.superlative.labs.wordwiz.R;

public class TutorialPagerAdapter extends FragmentPagerAdapter {

    private int[] images = new int[] {
            R.drawable.tutorial1,
            R.drawable.tutorial2,
            R.drawable.tutorial3,
            R.drawable.tutorial4,
            R.drawable.tutorial5,
            R.drawable.tutorial6,
            R.drawable.tutorial7,
            R.drawable.tutorial8,
            R.drawable.tutorial9,
            R.drawable.tutorial10,
    };

    private String[] descriptions = new String[] {
            "In WordWiz you’ll master the arcane art of vocabulary building by coming up with words that follow certain rules.",
            "To complete a level you’ll have to solve a number challenges. Each challenge will have its own set of rules.",
            "Within a level, you can move through the challenges by swiping or tapping the arrow buttons.",
            "There are many different challenges to overcome and rules to follow. It will get harder as you advance!",
            "Watch out for the clock! Time is limited. Try to obtain the highest score by completing each level as fast as you can.",
            "Hit the stop button when you’ve solved all the challenges.",
            "Need a little help? Hit the spellcheck button to check if your word exists or the clock plus button if you need more time.",
            "Once you complete a level, you can see how you did in detail.",
            "Tap on each word to see what you got right and what you got wrong on every specific challenge.",
            "Don’t forget to share your high score with your friends!"
    };

    public TutorialPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        TutorialPage fragment = new TutorialPage();

        Bundle args = new Bundle();
        args.putInt(TutorialPage.ARG_IMAGE_RES_ID, images[position]);
        args.putString(TutorialPage.ARG_DESCRIPTION, descriptions[position]);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return images.length;
    }
}