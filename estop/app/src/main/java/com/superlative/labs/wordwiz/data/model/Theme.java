package com.superlative.labs.wordwiz.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Catherine on 7/8/2015.
 */
@DatabaseTable
public class Theme {

    @DatabaseField(id = true)
    int id;

    @DatabaseField(index = true)
    String name;

    @DatabaseField
    String description;

    Theme() { }

    public Theme(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getDescription() {
        return description;
    }
}
