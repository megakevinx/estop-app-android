package com.superlative.labs.wordwiz.modifiers;

import com.superlative.labs.wordwiz.data.model.LevelChallengeModifier;
import com.superlative.labs.wordwiz.data.model.Modifier;

/**
 * Created by kevin on 9/27/2015.
 */
public class ModifierFactory {

//    private final WordRepository wordRepository;

//    public ModifierFactory(WordRepository wordRepository) {
//        this.wordRepository = wordRepository;
//    }

    public ModifierFactory() {

    }

    public IChallengeModifier createModifier(LevelChallengeModifier challengeModifier) {

        Modifier modifierType = challengeModifier.getModifier();

        switch (modifierType) {
//            case CategoryRequired:
//                return new CategoryRequired(challengeModifier.getId(), challengeModifier.getCategory(), this.wordRepository);

            case DoubleLetterRequired:
                return new DoubleLetterRequired(challengeModifier.getId(), challengeModifier.getLetter());

            case InitialLetterRequired:
                return new InitialLetterRequired(challengeModifier.getId(), challengeModifier.getLetter());

            case LetterProhibited:
                return new LetterProhibited(challengeModifier.getId(), challengeModifier.getLetter());

            case LetterRequired:
                return new LetterRequired(challengeModifier.getId(), challengeModifier.getLetter());

            case LetterUseBonus:
                return new LetterUseBonus(challengeModifier.getId(), challengeModifier.getLetter());

            case LetterUsePenalty:
                return new LetterUsePenalty(challengeModifier.getId(), challengeModifier.getLetter());

            case LastVowelRequired:
                return new LastVowelRequired(challengeModifier.getId(), challengeModifier.getLetter());

            case InitialVowelRequired:
                return new InitialVowelRequired(challengeModifier.getId(), challengeModifier.getLetter());

            case MiddleLetterRequired:
                return new MiddleLetterRequired(challengeModifier.getId(), challengeModifier.getLetter());

            case Palindrome:
                return new Palindrome(challengeModifier.getId(), challengeModifier.getLetter());

            case VowelNumberRequired:
                return new VowelNumberRequired(challengeModifier.getId(), challengeModifier.getLetter());

            case WordSizeRequired:
                return new WordSizeRequired(challengeModifier.getId(), challengeModifier.getLetter());

            case LastLetterRequired:
                return new LastLetterRequired(challengeModifier.getId(), challengeModifier.getLetter());
        }

        return null;
    }
}
