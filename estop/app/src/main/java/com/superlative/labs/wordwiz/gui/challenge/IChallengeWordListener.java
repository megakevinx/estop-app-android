package com.superlative.labs.wordwiz.gui.challenge;

public interface IChallengeWordListener {
    void notifyChallengeWordTextChanged(String text, int challenge);
}
