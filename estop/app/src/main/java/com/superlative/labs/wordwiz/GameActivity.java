package com.superlative.labs.wordwiz;

import java.util.LinkedHashMap;
import java.util.List;

import com.google.android.gms.ads.AdRequest;
import com.superlative.labs.wordwiz.gui.challenge.ChallengeWordFragment;
import com.superlative.labs.wordwiz.gui.challenge.IChallengeWordListener;
import com.superlative.labs.wordwiz.gui.challenge.ChallengePagerAdapter;
import com.superlative.labs.wordwiz.engine.GameManager;
import com.superlative.labs.wordwiz.engine.GameManager.IGameManagerHost;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.gui.level.LevelIconProvider;
import com.superlative.labs.wordwiz.round.FinishRoundData;
import com.superlative.labs.wordwiz.utils.ApplicationData;
import com.superlative.labs.wordwiz.utils.Constants;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;

public class GameActivity
        extends BaseGameActivity
        implements IChallengeWordListener,
                   IGameManagerHost, View.OnClickListener {

    InterstitialAd mInterstitialAd;

    ChallengePagerAdapter challengePagerAdapter;
    ViewPager viewPager;

    private static int defaultLevelId = 1;

    MediaPlayer btnMediaPlayer;
    private boolean getTimePlusActivityCalled = false;
    private Animation scaleButtonAnimation;
    private Animation scaleNextNavigationButtonAnimation;
    private Animation scalePrevNavigationButtonAnimation;

    /* Game Helper related stuff */
    @Override
    public void onSignInFailed() {
        Log.i(LOG_TAG, "FUCK FUCK FUCK FAILED FUCK");
    }

    @Override
    public void onSignInSucceeded() {
        Log.i(LOG_TAG, "FUCK SIGNED IN");
    }

    private final String LOG_TAG = getClass().getSimpleName();
    GameManager gameManager;

    private TextView get_textView_CurrentTime() {
        return (TextView)this.findViewById(R.id.textView_CurrentTime);
    }

    // Activity Methods
    @Override
    public void onBackPressed() { } //Let's prevent the default behavior of the Back Button
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        this.init();

        this.initAd();

        this.gameManager.beginRound();
    }

    private void init() {
        this.initKeyboard();

        Intent intent = getIntent();

        String gameLoadMode = intent.getStringExtra(Keys.IntentParameters.GameLoadMode);

        int levelId = 0;

        if (gameLoadMode != null) {
            if (gameLoadMode.equals(Keys.IntentParameters.GameLoadModes.LevelSelected)) {

                levelId = intent.getIntExtra(Keys.IntentParameters.LevelId, GameActivity.defaultLevelId);
                this.gameManager = new GameManager(this, levelId);
            }
        }
        else {
            this.gameManager = new GameManager(this, defaultLevelId);
        }

        this.initLevelInfo();

        this.initWordsForChallenge(this.gameManager.getChallengesIds());

        challengePagerAdapter =
                new ChallengePagerAdapter(
                        getSupportFragmentManager(),
                        this.gameManager.getChallengesIds()
                );

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(challengePagerAdapter);
        viewPager.setOffscreenPageLimit(this.gameManager.getChallengesIds().size());

        this.scaleButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down);
        this.scaleNextNavigationButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down_pivot_right);
        this.scalePrevNavigationButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down_pivot_left);
    }

    private void initLevelInfo() {

        LevelIconProvider levelIconProvider = new LevelIconProvider();

        int levelIconResId = levelIconProvider.getIconResourceId(this.gameManager.getLevelId());
        String levelName = this.gameManager.getLevelName();

        ImageView imageViewLevelIcon = (ImageView) this.findViewById(R.id.imageView_LevelIcon);
        TextView textViewLevelName = (TextView) this.findViewById(R.id.textView_LevelName);

        imageViewLevelIcon.setImageResource(levelIconResId);
        textViewLevelName.setText(levelName);
    }

    private void initAd() {

        boolean removeAds = this.gameManager.getRemoveAds();

        if (!removeAds) {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.intersitial_ad_unit_id));

            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    requestNewInterstitial();
                    gameManager.finishRound();
                }
            });

            requestNewInterstitial();
        }
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(Constants.DEVICE_ID)
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private void initKeyboard() {
        findViewById(R.id.btnA).setOnClickListener(this);
        findViewById(R.id.btnB).setOnClickListener(this);
        findViewById(R.id.btnC).setOnClickListener(this);
        findViewById(R.id.btnD).setOnClickListener(this);
        findViewById(R.id.btnE).setOnClickListener(this);
        findViewById(R.id.btnF).setOnClickListener(this);
        findViewById(R.id.btnG).setOnClickListener(this);
        findViewById(R.id.btnH).setOnClickListener(this);
        findViewById(R.id.btnI).setOnClickListener(this);
        findViewById(R.id.btnJ).setOnClickListener(this);
        findViewById(R.id.btnK).setOnClickListener(this);
        findViewById(R.id.btnL).setOnClickListener(this);
        findViewById(R.id.btnM).setOnClickListener(this);
        findViewById(R.id.btnN).setOnClickListener(this);
        findViewById(R.id.btnO).setOnClickListener(this);
        findViewById(R.id.btnP).setOnClickListener(this);
        findViewById(R.id.btnQ).setOnClickListener(this);
        findViewById(R.id.btnR).setOnClickListener(this);
        findViewById(R.id.btnS).setOnClickListener(this);
        findViewById(R.id.btnT).setOnClickListener(this);
        findViewById(R.id.btnU).setOnClickListener(this);
        findViewById(R.id.btnV).setOnClickListener(this);
        findViewById(R.id.btnW).setOnClickListener(this);
        findViewById(R.id.btnX).setOnClickListener(this);
        findViewById(R.id.btnY).setOnClickListener(this);
        findViewById(R.id.btnZ).setOnClickListener(this);
        findViewById(R.id.backspace).setOnClickListener(this);
    }

    LinkedHashMap<Integer, String> wordsForChallenge;

    public void initWordsForChallenge(List<Integer> challengesIds) {

        this.wordsForChallenge = new LinkedHashMap<>();

        for (Integer challengeId : challengesIds) {
            this.wordsForChallenge.put(challengeId, "");
        }
    }

    public void notifyChallengeWordTextChanged(String text, int challenge) {
        this.wordsForChallenge.put(challenge, text);

        Log.println(Log.DEBUG, "stop", this.wordsForChallenge.get(challenge));
    }

    // Event Handlers
    public void button_FinishRound_OnClick(View sender) {

        this.gameManager.finishRound();

        boolean removeAds = this.gameManager.getRemoveAds();

        if (!removeAds) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                this.gameManager.finishRound();
            }
        }
        else {
            this.gameManager.finishRound();
        }
    }

    public void imageButton_PreviousChallenge_OnClick(View sender) {

        sender.startAnimation(this.scalePrevNavigationButtonAnimation);

        int numberOfChallenges = this.gameManager.getChallengesIds().size();

        int currentChallengeIndex = this.viewPager.getCurrentItem();

        if (currentChallengeIndex > 0) {
            this.viewPager.setCurrentItem(currentChallengeIndex - 1);
        } else{
            this.viewPager.setCurrentItem(numberOfChallenges - 1);
        }
    }

    public void imageButton_NextChallenge_OnClick(View sender) {

        sender.startAnimation(this.scaleNextNavigationButtonAnimation);

        int currentChallengeIndex = this.viewPager.getCurrentItem();
        int numberOfChallenges = this.gameManager.getChallengesIds().size();

        Log.i(LOG_TAG, "CurrentCallengeIndex: " + currentChallengeIndex);
        if (currentChallengeIndex < numberOfChallenges - 1) {
            this.viewPager.setCurrentItem(currentChallengeIndex + 1);
        }
        else{
            this.viewPager.setCurrentItem(0);
        }
    }

    public void button_IncreaseTime_OnClick(View sender) {
        sender.startAnimation(this.scaleButtonAnimation);
        this.gameManager.tryIncreaseTime();
    }

    public void button_CheckWord_OnClick(View sender) {

        sender.startAnimation(this.scaleButtonAnimation);

        if (this.gameManager.currentWordExists()) {
            showToast("This word exists!");
        }
        else
        {
            showToast("This word does not exist!");
        }
    }

    private void showToast(String message) {
        Context context = getApplicationContext();
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    
    // IGameManagerHost Methods
    public Context getContext() {
        return this;
    }
    
    @Override
    public LinkedHashMap<Integer, String> getPlayedWords() {
        return this.wordsForChallenge;
    }

    public int getCurrentChallengeId() {

        int currentItemIndex = this.viewPager.getCurrentItem();

        return (int) this.challengePagerAdapter.getItemId(currentItemIndex);
    }

    @Override
    public void notifyRoundFinished(FinishRoundData submittedData)
    {
        Log.i(LOG_TAG, "notifyRoundFinished");
        this.startScoreActivity(submittedData);
    }

    private void startScoreActivity(FinishRoundData submittedData) {

        Intent intent = new Intent(this, ScoreActivity.class);
        intent.putExtra(Keys.IntentParameters.FinishRoundData, submittedData);

        this.startActivity(intent);
    }

    @Override
    public void notifySecondPassed(int secondsLeft) {
        this.get_textView_CurrentTime().setText(Integer.toString(secondsLeft));
    }

    @Override
    public void notifyTimeOver(boolean hasTimePlus) {

        this.getTimePlusActivityCalled = true;

        this.startGetTimePlusActivity(hasTimePlus);

        //this.gameManager.finishRound();
    }

    @Override
    public void onResume(){
        super.onResume();

        if (this.getTimePlusActivityCalled) {

            ApplicationData appData = (ApplicationData) getApplication();
            boolean useTimePlusPowerUp = appData.getUseTimePlusPowerUp();
            appData.resetUseTimePlusPowerUp();

            this.gameManager.tryContinueRound(useTimePlusPowerUp);
        }

        Log.i(LOG_TAG, "onResume YES!");
    }

    @Override
    public void notifyTimeIncreaseNotApplied() {
        this.showToast("You don't have any more Time Plus power ups!");
    }

    private void startGetTimePlusActivity(boolean hasTimePlus) {
        Intent intent = new Intent(this, GetTimePlusActivity.class);

        intent.putExtra(Keys.IntentParameters.HasTimePlus, hasTimePlus);

        this.startActivity(intent);
    }

    @Override
    public void setTimePlusPowerUpNumber(int number) {
        TextView timePlusQty = (TextView) this.findViewById(R.id.textView_TimePlusQuantity);
        timePlusQty.setText(Integer.toString(number));
    }

    @Override
    public void onClick(View pressedKey) {

        pressedKey.startAnimation(this.scaleButtonAnimation);

        ChallengeWordFragment fragment = challengePagerAdapter.getWordFragmentList().get(viewPager.getCurrentItem());
        String currentWord = fragment.getChallengeWord();

        btnMediaPlayer = MediaPlayer.create(this, R.raw.short_pop_sound);
        btnMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                mp.release();
                mp=null;
            }

        });
        btnMediaPlayer.start();

        if (pressedKey.getId() == R.id.backspace) {
            if(currentWord.length() > 0) {
                String newText = currentWord.substring(0, currentWord.length() - 1);
                fragment.setChallengeWord(newText);
            }
        }
        else {
            currentWord += ((Button)pressedKey).getText().toString();
            fragment.setChallengeWord(currentWord);
        }
    }
}