package com.superlative.labs.wordwiz.gui.stage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import com.superlative.labs.wordwiz.R;
import com.superlative.labs.wordwiz.data.model.Stage;

/**
 * Created by kevin on 11/9/2015.
 */
public class StagesListAdapter extends BaseAdapter implements ListAdapter {

    private final List<Stage> stages;
    private final Context context;
    private final StageIconProvider stageIconProvider;

    public StagesListAdapter(Context context, List<Stage> stages) {
        this.context = context;
        this.stages = stages;

        this.stageIconProvider = new StageIconProvider();
    }

    @Override
    public int getCount() {
        return this.stages.size();
    }

    @Override
    public Object getItem(int position) {
        return this.stages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.stages.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Stage stage = ((Stage) this.getItem(position));
        int layoutToLoad;

        if (stage.getIsLocked()) {
            layoutToLoad = R.layout.list_item_stage_locked;
        }
        else {
            layoutToLoad = R.layout.list_item_stage_unlocked;
        }

        //if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutToLoad, parent, false);
        //}

        TextView textViewName = (TextView) convertView.findViewById(R.id.textView_StageName);
        ImageView imageViewIcon = (ImageView) convertView.findViewById(R.id.imageView_Icon);

        String name = stage.getName();
        Integer iconResourceId = this.stageIconProvider.getIconResourceId(stage);
        //String locked = stage.getIsLocked() ? "Locked" : "Unlocked";

        textViewName.setText(name);
        imageViewIcon.setImageResource(iconResourceId);
        //textViewIsLocked.setText(locked);

        return convertView;
    }
}
