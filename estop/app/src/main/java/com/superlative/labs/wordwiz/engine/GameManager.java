package com.superlative.labs.wordwiz.engine;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Level;
import com.superlative.labs.wordwiz.data.model.LevelChallenge;
import com.superlative.labs.wordwiz.data.repositories.LevelRepository;
import com.superlative.labs.wordwiz.data.repositories.PowerUpRepository;
import com.superlative.labs.wordwiz.data.repositories.WordRepository;
import com.superlative.labs.wordwiz.engine.CountdownTask.ICountdownProgressObserver;
import com.superlative.labs.wordwiz.round.FinishRoundData;

public class GameManager implements ICountdownProgressObserver
{
    public interface IGameManagerHost
    {
        LinkedHashMap<Integer, String> getPlayedWords();
        
        //void notifyLetterSet(String currentLetter);

        void notifyRoundFinished(FinishRoundData submittedData);
        
        void notifySecondPassed(int secondsLeft);
        void notifyTimeOver(boolean hasTimePlus);
        void notifyTimeIncreaseNotApplied();

        void setTimePlusPowerUpNumber(int number);

        Context getContext();

        int getCurrentChallengeId();
    }
    
    IGameManagerHost host;
        
    CountdownTask countdown;

//    String currentLetter;
    int countdownSeconds;
    int secondsLeft;

    int levelId;
    String levelName;

    List<LevelChallenge> challenges;

    private PowerUpRepository powerUpRepository;
    private WordRepository wordRepository;


    public GameManager(IGameManagerHost host, int levelId) {

        this.host = host;
        this.levelId = levelId;

        this.loadLevel(levelId);
    }

    private void loadLevel(int levelId) {

        DatabaseHelper db = new DatabaseHelper(this.host.getContext());
        LevelRepository levelRepository = new LevelRepository(db);

        this.powerUpRepository = new PowerUpRepository(db);
        this.wordRepository = new WordRepository(db);

        this.host.setTimePlusPowerUpNumber(this.powerUpRepository.getCurrentTimePlus());

        Level levelToLoad = levelRepository.getById(levelId);

        this.levelName = levelToLoad.getName();
        this.countdownSeconds = levelToLoad.getSeconds();

        // TODO: create a method that, given a theme, generates a list of LevelChallenge.
        // Each challenge will have an associated list of LevelChallengeModifier as well.
        this.challenges = new ArrayList<>(levelToLoad.getLevelChallenges());
    }

    public List<Integer> getChallengesIds() {
        List<Integer> challengesToReturn = new ArrayList<>();

        for (LevelChallenge challenge : this.challenges)
        {
            challengesToReturn.add(challenge.getId());
        }

        return challengesToReturn;
    }
    
    public void beginRound() {
        //this.setLetter();
        this.startCountdown();
    }
    
//    private void setLetter() {
//
//        // TODO: Dont treat InitialLetterRequired as special
//        for (LevelChallengeModifier modifier : this.challenges.get(0).getLevelChallengeModifiers()) {
//            if (modifier.getModifier() == Modifier.LetterRequired) {
//                this.currentLetter = modifier.getLetter();
//            }
//        }
//
//        this.host.notifyLetterSet(this.currentLetter);
//    }
    
    private void startCountdown() {
        if(this.countdown != null)
        {
            this.countdown.cancel(true);
        }
        
        this.countdown = new CountdownTask(this, this.countdownSeconds);
        
        this.countdown.execute();
    }

    public void tryIncreaseTime() {
        if (this.powerUpRepository.getCurrentTimePlus() > 0) {
            this.increaseTime(Keys.Products.SecondsIncrease.Ten);
        }
        else {
            this.host.notifyTimeIncreaseNotApplied();
        }
    }

    private void increaseTime(int secondsToIncrease) {
        this.countdown.increaseSecondsLeft(secondsToIncrease);
        this.powerUpRepository.decreasePowerUpQuantity(Keys.Products.Skus.TimePlus, Keys.Products.UseQuantity.One);

        this.host.setTimePlusPowerUpNumber(this.powerUpRepository.getCurrentTimePlus());
    }

    public void finishRound() {
        this.countdown.cancel(true);

        LinkedHashMap<Integer, String> playedWordsForChallengeId = this.host.getPlayedWords();

        FinishRoundData roundData = new FinishRoundData(
                this.levelId,
                playedWordsForChallengeId,
                this.secondsLeft
        );

        this.host.notifyRoundFinished(roundData);
    }

    // ICountdownProgressObserver Methods
    @Override
    public void notifyCountdownInit() {
        this.host.notifySecondPassed(this.countdownSeconds);
    }

    @Override
    public void notifySecondPassed(int secondsLeft) {
        this.secondsLeft = secondsLeft;
        this.host.notifySecondPassed(secondsLeft);
    }

    @Override
    public void notifyCountdownOver() {

        this.countdown.cancel(true);

        boolean hasTimePlus = this.powerUpRepository.getCurrentTimePlus() > 0;

        this.host.notifyTimeOver(hasTimePlus);
    }

    public void tryContinueRound(boolean useTimePlusPowerUp) {

        if (useTimePlusPowerUp && this.powerUpRepository.getCurrentTimePlus() > 0) {
            this.countdown = new CountdownTask(this);
            this.increaseTime(Keys.Products.SecondsIncrease.Ten);
            this.countdown.execute();
        }
        else {
            this.finishRound();
        }
    }

    public boolean currentWordExists() {

        int currentChallengeId = this.host.getCurrentChallengeId();

        return this.wordRepository.wordExists(this.host.getPlayedWords().get(currentChallengeId));
    }

    public boolean getRemoveAds() {
        return this.powerUpRepository.getHasRemovedAds();
    }

    public int getLevelId() {
        return this.levelId;
    }

    public String getLevelName() {
        return this.levelName;
    }

}
