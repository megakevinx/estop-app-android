package com.superlative.labs.wordwiz.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Catherine on 6/23/2015.
 */
@DatabaseTable
public class Category {

    @DatabaseField(id = true)
    int id;

    @DatabaseField(index = true)
    String name;

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    Theme theme;

    Category() { }

    public Category(int id, String name, Theme theme) {
        this.id = id;
        this.name = name;
        this.theme = theme;
    }

    public int getId() {
        return id;
    }

    public String getName(){
        return name;
    }

    public Theme getTheme() {
        return theme;
    }
}
