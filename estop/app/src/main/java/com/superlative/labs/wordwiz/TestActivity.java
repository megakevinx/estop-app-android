package com.superlative.labs.wordwiz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.ArrayList;
import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Category;
import com.superlative.labs.wordwiz.data.model.Level;
import com.superlative.labs.wordwiz.data.model.LevelChallenge;
import com.superlative.labs.wordwiz.data.model.LevelChallengeModifier;
import com.superlative.labs.wordwiz.data.model.Stage;
import com.superlative.labs.wordwiz.data.model.Theme;
import com.superlative.labs.wordwiz.data.model.Word;
import com.superlative.labs.wordwiz.data.repositories.ILevelRepository;
import com.superlative.labs.wordwiz.engine.AchievementsManager;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.round.FinishRoundData;
import com.superlative.labs.wordwiz.utils.achievements.IAchievementApiClient;
import com.superlative.labs.wordwiz.utils.billing.IabHelper;
import com.superlative.labs.wordwiz.utils.billing.IabResult;
import com.superlative.labs.wordwiz.utils.billing.Inventory;
import com.superlative.labs.wordwiz.utils.billing.Purchase;
import com.superlative.labs.wordwiz.utils.billing.SkuDetails;

public class TestActivity extends Activity {

    IabHelper mHelper;

    private String TAG = "TestActivity";

    private String SKU_TEST = "android.test.purchased";
    private String SKU_001 = "tst001";
    private String SKU_002 = "tst002";

    TextView testResult;

    public class MockAchievementsManagerHost implements AchievementsManager.IAchievementsManagerHost {

        boolean levelCompleted;
        ArrayList<String> playedWords;
        int secondsLeft;

        public MockAchievementsManagerHost(boolean levelCompleted, ArrayList<String> playedWords, int secondsLeft) {
            this.levelCompleted = levelCompleted;
            this.playedWords = playedWords;
            this.secondsLeft = secondsLeft;
        }

        @Override
        public boolean getLevelCompleted() {
            return this.levelCompleted;
        }

        @Override
        public ArrayList<String> getPlayedWords() {
            return this.playedWords;
        }

        @Override
        public int getSecondsLeft() {
            return this.secondsLeft;
        }
    }

    public class MockAchievementApiClient implements IAchievementApiClient {

        @Override
        public void unlock(String achievementCode) {
            Log.d("AchievementApiClient", "unlocked achievement: " + achievementCode);
            showToast("unlocked achievement: " + achievementCode);
        }
    }

    public class MockLevelRepository implements ILevelRepository {

        int totalScore;
        int completedLevelsCount;

        public MockLevelRepository(int totalScore, int completedLevelsCount) {
            this.totalScore = totalScore;
            this.completedLevelsCount = completedLevelsCount;
        }

        @Override
        public int getTotalScore() {
            return this.totalScore;
        }

        @Override
        public int getCompletedLevelsCount() {
            return this.completedLevelsCount;
        }

        @Override
        public void unlockNextLevel(Level level) {

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        testAchievements();



//        LinkedHashMap<Integer, String> playedWordsForChallengeId = new LinkedHashMap<>();
//        playedWordsForChallengeId.put(1, "SEE");
//        playedWordsForChallengeId.put(2, "DAD");
//        playedWordsForChallengeId.put(3, "ART");
//        playedWordsForChallengeId.put(4, "RAasdasdT");
//        playedWordsForChallengeId.put(5, "SOON");
//
//        FinishRoundData roundData = new FinishRoundData(1, playedWordsForChallengeId, 100);
//
//        this.startScoreActivity(roundData);

//        Intent intent = new Intent(this, GameActivity.class);
//        intent.putExtra(Keys.IntentParameters.GameLoadMode, Keys.IntentParameters.GameLoadModes.LevelSelected);
//        intent.putExtra(Keys.IntentParameters.LevelId, 1);
//
//        this.startActivity(intent);

//        this.testResult = (TextView)this.findViewById(R.id.textView_TestResult);
//
//        final TextView closureTestResult = this.testResult;
//
//        // ...
//        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAshyoLF4UKKO8U+b6Vw9N2ouJeHeQZWVv3HrvR3mFFuvzh19HeijXZNe3jaD5qpV2JY4a2ZHrADBkcam6QRHwjpH0DcbUENmG0bhpIrl4ab2pPf2tiWx9B6ePYS3y01mnDJul0N7PxnoAYNBkyZr22Rcnc9sbOmvN8DIQmn2HMHgX+xxalKFiKwuKmPqsbAH3iZLpnIakj8Y2wJ2piwUzLHvrHInSPRhTM+g2E0TD/8g6x5h6X5Sw1mkf4vxwNjaEcYRcwQVlVc8XshDjfzD28FUMe+MWEJ3xzBpkEGzt0T4TDtmH7LCPfyLB2YdodbEpkIkaTpeTGzUCZJAKlncrXQIDAQAB";
//
//        // compute your public key and store it in base64EncodedPublicKey
//        mHelper = new IabHelper(this, base64EncodedPublicKey);
//
//        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//
//            public void onIabSetupFinished(IabResult result) {
//                if (!result.isSuccess()) {
//                    // Oh noes, there was a problem.
//                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
//                    closureTestResult.append("Problem setting up In-app Billing: " + result);
//
//                }
//                // Hooray, IAB is fully set up!
//                Log.d(TAG, "HOLY SHIT IT WORKS!");
//                closureTestResult.append("HOLY SHIT IT WORKS!");
//            }
//        });
    }

    public void testAchievements() {

        boolean levelCompleted = true;
        int secondsLeft = 15;
        int totalScore = 100;
        int completedLevelsCount = 5;

        ArrayList<String> playedWords = new ArrayList<>();
        playedWords.add("MOTHER");
        playedWords.add("B");
        playedWords.add("C");

        Log.d("test", "1st level");
        AchievementsManager testManager =
                new AchievementsManager(
                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
                        new MockAchievementApiClient(),
                        new MockLevelRepository(totalScore, completedLevelsCount));

        testManager.checkAchievements();


        playedWords = new ArrayList<>();
        playedWords.add("ERRONEOUS");
        playedWords.add("B");
        playedWords.add("C");

        Log.d("test", "1st level");
        testManager =
                new AchievementsManager(
                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
                        new MockAchievementApiClient(),
                        new MockLevelRepository(totalScore, completedLevelsCount));

        testManager.checkAchievements();

//        Log.d("test", "");
//        completedLevelsCount = 10;
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//        Log.d("test", "");
//        completedLevelsCount = 400;
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//        Log.d("test", "");
//        completedLevelsCount = 5;
//        totalScore = 5000;
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//
//        Log.d("test", "");
//        completedLevelsCount = 5;
//        totalScore = 80000;
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//
//        Log.d("test", "");
//        completedLevelsCount = 5;
//        totalScore = 140000;
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//
//        Log.d("test", "");
//        levelCompleted = true;
//        completedLevelsCount = 5;
//        totalScore = 500;
//        secondsLeft = 4;
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//        Log.d("test", "");
//        levelCompleted = true;
//        completedLevelsCount = 5;
//        totalScore = 500;
//        secondsLeft = 51;
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//
//        Log.d("test", "");
//        completedLevelsCount = 5;
//        totalScore = 500;
//        secondsLeft = 15;
//        playedWords = new ArrayList<>();
//        playedWords.add("1234512345");
//        playedWords.add("B");
//        playedWords.add("C");
//
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//        Log.d("test", "");
//        completedLevelsCount = 5;
//        totalScore = 500;
//        secondsLeft = 15;
//        playedWords = new ArrayList<>();
//        playedWords.add("123451234512345");
//        playedWords.add("B");
//        playedWords.add("C");
//
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//
//        Log.d("test", "");
//        completedLevelsCount = 5;
//        totalScore = 500;
//        secondsLeft = 15;
//        playedWords = new ArrayList<>();
//        playedWords.add("123");
//        playedWords.add("123");
//        playedWords.add("123");
//
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//
//        Log.d("test", "");
//        completedLevelsCount = 5;
//        totalScore = 500;
//        secondsLeft = 15;
//        playedWords = new ArrayList<>();
//        playedWords.add("12345");
//        playedWords.add("12345");
//        playedWords.add("12345");
//
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
//
//
//
//        Log.d("test", "");
//        completedLevelsCount = 5;
//        totalScore = 500;
//        secondsLeft = 15;
//        playedWords = new ArrayList<>();
//        playedWords.add("123451234512345");
//        playedWords.add("123451234512345");
//        playedWords.add("123451234512345");
//
//        testManager =
//                new AchievementsManager(
//                        new MockAchievementsManagerHost(levelCompleted, playedWords, secondsLeft),
//                        new MockAchievementApiClient(),
//                        new MockLevelRepository(totalScore, completedLevelsCount));
//
//        testManager.checkAchievements();
    }

    private void showToast(String message) {
        Context context = getApplicationContext();
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private void startScoreActivity(FinishRoundData submittedData) {

        // TODO: We will need some way of telling the ScoreActivity that the level was
        // loaded from a theme so that it knows to expect the new object (not FinishRoundData)
        // that includes the Words along with the LevelChallenge and LevelChallengeModifier objects.
        Intent intent = new Intent(this, ScoreActivity.class);
        intent.putExtra(Keys.IntentParameters.FinishRoundData, submittedData);

        this.startActivity(intent);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();

        if (mHelper != null) mHelper.dispose();

        mHelper = null;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_test, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    public void button_ExecuteIabQueryTest_OnClick(View sender) {

        List<String> additionalSkuList = new ArrayList<>();
        //additionalSkuList.add(SKU_TEST);
        additionalSkuList.add(SKU_001);
        additionalSkuList.add(SKU_002);

        final TextView closureTestResult = this.testResult;

        mHelper.queryInventoryAsync(true, additionalSkuList, new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory)
            {
                if (result.isFailure()) {
                    // handle error
                    Log.d(TAG, "well fuck");
                    closureTestResult.append("well fuck");
                    return;
                }

                //SkuDetails tst001 = inventory.getSkuDetails(SKU_TEST);
                SkuDetails tst001 = inventory.getSkuDetails(SKU_001);
                SkuDetails tst002 = inventory.getSkuDetails(SKU_002);

                if (tst001 != null) {
                    Log.d(TAG, "\ntst1 title: " + tst001.getTitle());
                    closureTestResult.append("\ntst1 title: " + tst001.getTitle());

                    boolean hasPurchase = inventory.hasPurchase(SKU_001);

                    Log.d(TAG, "\ntst1 hasPurchase: " + hasPurchase);
                    closureTestResult.append("\ntst1 hasPurchase: " + hasPurchase);
                }
                else {
                    Log.d(TAG, "\nfuck no txt1 title\n");
                    closureTestResult.append("fuck no txt1 title\n");
                }

                if (tst002 != null) {
                    Log.d(TAG, "\ntst2 title: " + tst002.getTitle());
                    closureTestResult.append("\ntst2 title: " + tst002.getTitle());

                    boolean hasPurchase = inventory.hasPurchase(SKU_002);

                    Log.d(TAG, "\ntst2 hasPurchase: " + hasPurchase);
                    closureTestResult.append("\ntst2 hasPurchase: " + hasPurchase);
                }
                else {
                    Log.d(TAG, "fuck no tst2Price");
                    closureTestResult.append("fuck no tst2Price");
                }

                // update the UI
            }
        });
    }

    public void button_ExecuteIabPurchaseTest_OnClick(View sender) {

        final TextView closureTestResult = this.testResult;

        IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase)
            {
                if (result.isFailure()) {
                    Log.d(TAG, "\nError purchasing: " + result);
                    closureTestResult.append("\nError purchasing: " + result);
                    return;
                }
                else if (purchase.getSku().equals(SKU_TEST)) {
                    Log.d(TAG, "IT FUKIN WORKS: " + result);
                    closureTestResult.append("\nIT FUKIN WORKS: " + result);
                }
            }
        };

        mHelper.launchPurchaseFlow(this, SKU_TEST, 10001, mPurchaseFinishedListener, "some random string shit");
    }

    public void button_ExecuteTest_OnClick(View sender)
    {
        DatabaseHelper helper = new DatabaseHelper(this);
        RuntimeExceptionDao<Stage, Integer> stageDao = helper.getStageDao();
        RuntimeExceptionDao<Level, Integer> levelDao = helper.getLevelDao();
        RuntimeExceptionDao<LevelChallenge, Integer> levelchallengeDao = helper.getLevelChallengeDao();
        RuntimeExceptionDao<LevelChallengeModifier, Integer> levelChallengeModifierDao =
                helper.getLevelChallengeModifierDao();
        RuntimeExceptionDao<Theme, Integer> themeDao = helper.getThemeDao();
        RuntimeExceptionDao<Category, Integer> categoryDao = helper.getCategoryDao();
        RuntimeExceptionDao<Word, Integer> wordDao = helper.getWordDao();

        List<Stage> stages = stageDao.queryForAll();

        Log.d("database_test", "\n\nStages\n\n");

        for (Stage stage : stages) {
            Log.d("database_test",
                    stage.getId() + " - " +
                    stage.getSequence() + " - " +
                    stage.getName() + "     ");
        }

        Log.d("database_test", "\n\nStages\n\n");

        List<Level> levels = levelDao.queryForAll();

        for (Level level : levels) {
            Log.d("database_test",
                    level.getId() + " - " +
                    level.getSequence() + " - " +
                    level.getStage().getName() + " - " +
                    level.getName() + " - " +
                    level.getSeconds() + "     ");
        }

        Log.d("database_test", "\n\nLevel Challenges\n\n");

        List<LevelChallenge> levelChallenges = levelchallengeDao.queryForAll();

        for (LevelChallenge levelChallenge : levelChallenges) {
            Log.d("database_test",
                    levelChallenge.getId() + " - " +
                    levelChallenge.getLevel().getName() + "     ");
        }

        Log.d("database_test", "\n\nLevel Challenge Modifiers\n\n");

        List<LevelChallengeModifier> levelChallengeModifiers = levelChallengeModifierDao.queryForAll();

        for (LevelChallengeModifier levelChallengeModifier : levelChallengeModifiers) {
            Log.d("database_test",
                    levelChallengeModifier.getId() + " - " +
                    levelChallengeModifier.getLevelChallenge().getLevel().getName() + " - " +
                    levelChallengeModifier.getModifier().toString() + " - " +
                    levelChallengeModifier.getLetter() + " - " +
                    levelChallengeModifier.getCategory().getName() + " - " +
                    levelChallengeModifier.getValue() + "     ");
        }

        List<Theme> themes = themeDao.queryForAll();

        Log.d("database_test", "\n\nThemes\n\n");

        for (Theme theme : themes) {
            Log.d("database_test",
                    theme.getId() + " - " +
                    theme.getName() + " - " +
                    theme.getDescription() + "     ");
        }

        List<Category> categories = categoryDao.queryForAll();

        Log.d("database_test", "\n\nCategories\n\n");

        for (Category category : categories) {
            Log.d("database_test",
                    category.getId() + " - " +
                    category.getName() + " - " +
                    category.getTheme().getName() + "     ");
        }

        List<Word> words = wordDao.queryForAll();

        Log.d("database_test", "\n\nWords\n\n");

        for (Word word : words) {
            Log.d("database_test",
                    word.getId() + " - " +
                    word.getName() + " - " +
                    word.getCategory().getName() + "     ");
        }
    }
}
