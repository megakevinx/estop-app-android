package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class RateAppActivity extends BaseGameActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_app);
    }

    @Override
    public void onSignInFailed() { }

    @Override
    public void onSignInSucceeded() { }

    public void button_RateApp_OnClick(View sender) {
        this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + Keys.PackageName)));
        this.finish();
    }

    public void button_RemindMeLater_OnClick(View sender) {
        SharedPreferences prefs = this.getSharedPreferences("apprater", 0);
        SharedPreferences.Editor editor = prefs.edit();

        if (editor != null) {
            editor.putLong("launch_count", 0);
            editor.commit();
        }

        this.finish();
    }

    public void button_DontShowAgain_OnClick(View sender) {

        SharedPreferences prefs = this.getSharedPreferences("apprater", 0);
        SharedPreferences.Editor editor = prefs.edit();

        if (editor != null) {
            editor.putBoolean("dontshowagain", true);
            editor.commit();
        }

        this.finish();
    }
}
