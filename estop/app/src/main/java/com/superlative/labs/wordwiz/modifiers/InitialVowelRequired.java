package com.superlative.labs.wordwiz.modifiers;

import android.util.Log;

/**
 * Created by Catherine on 11/25/2015.
 */
public class InitialVowelRequired extends BaseChallengeModifier {

    private String letter;

    public InitialVowelRequired(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        char firstCharacter = word.charAt(0);
        Log.i("InitialVowelRequired", "First character: " + firstCharacter);
        return "aeiou".contains(Character.toString(firstCharacter).toLowerCase());
    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return "First Letter is a vowel";
    }

    @Override
    public String toString() {
        return "Starts with vowel";
    }
}
