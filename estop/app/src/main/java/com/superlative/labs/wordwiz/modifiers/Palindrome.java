package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by Catherine on 11/25/2015.
 */
public class Palindrome extends BaseChallengeModifier  {

    private String letter;

    public Palindrome(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        return isPalindrome(word.toCharArray());
    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Is a Palindrome";
    }

    public static boolean isPalindrome(char[] word){
        int i1 = 0;
        int i2 = word.length - 1;
        while (i2 > i1) {
            if (word[i1] != word[i2]) {
                return false;
            }
            ++i1;
            --i2;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Palindrome";
    }
}
