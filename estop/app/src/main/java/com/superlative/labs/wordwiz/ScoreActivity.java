package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Level;
import com.superlative.labs.wordwiz.data.model.LevelChallengeModifier;
import com.superlative.labs.wordwiz.data.model.Stage;
import com.superlative.labs.wordwiz.data.repositories.LevelChallengeRepository;
import com.superlative.labs.wordwiz.data.repositories.LevelRepository;
import com.superlative.labs.wordwiz.data.repositories.PowerUpRepository;
import com.superlative.labs.wordwiz.data.repositories.StageRepository;
import com.superlative.labs.wordwiz.data.repositories.WordRepository;
import com.superlative.labs.wordwiz.engine.AchievementsManager;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.gui.score.ChallengeResultsListAdapter;
import com.superlative.labs.wordwiz.modifiers.IChallengeModifier;
import com.superlative.labs.wordwiz.modifiers.ModifierFactory;
import com.superlative.labs.wordwiz.round.ChallengeScore;
import com.superlative.labs.wordwiz.round.FinishRoundData;
import com.superlative.labs.wordwiz.round.RoundScore;
import com.superlative.labs.wordwiz.utils.Constants;
import com.superlative.labs.wordwiz.utils.achievements.AchievementApiClient;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

// TODO: Refactor score calculation logic into a new ScoreManager class.
public class ScoreActivity extends BaseGameActivity
        implements Animation.AnimationListener,
        AchievementsManager.IAchievementsManagerHost {

    // request codes we use when invoking an external activity
    private static final int RC_UNUSED = 5001;
    private int SHARE_REQUEST_CODE = 100;

    final String LOG_TAG = "ScoreActivity";

    private long oldScore;
    private long newScore;
    private long leaderboardScore;

    private WordRepository wordRepository;
    private LevelChallengeRepository levelChallengeRepository;
    private LevelRepository levelRepository;
    private StageRepository stageRepository;
    private PowerUpRepository powerUpRepository;

    private ModifierFactory modifierFactory;

    private RoundScore roundScore;
    private FinishRoundData finishRoundData;

    private int currentLevelId;

    private static final int TIME_SCORE_MULTIPLIER = 10;
    private static final int CHALLENGE_SUCCESS_SCORE = 100;
    private boolean outOfLives = false;

    private TextView textViewLevelScore;
    AlphaAnimation fadeIn, fadeOut;

    private static int count = 0, finalValue=0;
    private AchievementsManager achievementsManager;

    Animation scaleButtonAnimation;

    private boolean mIsBound = false;

    String screenShotFilePath = "";

    @Override
    public void onBackPressed() { } //Let's prevent the default behavior of the Back Button
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_score);

        this.initialize();

        this.displayAd();

        this.finishRoundData = this.getIntent().getParcelableExtra(Keys.IntentParameters.FinishRoundData);

        this.currentLevelId = this.finishRoundData.getLevelId();

        this.roundScore = this.calculateScore(this.finishRoundData);

        this.completeLevel(this.roundScore, this.finishRoundData);

        this.showRoundResultPreview(this.roundScore);
    }

    private void initialize() {

        DatabaseHelper dbHelper = new DatabaseHelper(this);

        this.wordRepository = new WordRepository(dbHelper);

        this.levelChallengeRepository = new LevelChallengeRepository(dbHelper);
        this.stageRepository = new StageRepository(dbHelper);
        this.levelRepository = new LevelRepository(dbHelper);
        this.powerUpRepository = new PowerUpRepository(dbHelper);

        this.modifierFactory = new ModifierFactory();

        this.scaleButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down_pivot_top);

        this.achievementsManager =
                new AchievementsManager(
                        this,
                        new AchievementApiClient(this.getApiClient()),
                        this.levelRepository);
    }

    private void displayAd() {

        boolean hasRemovedAds = this.powerUpRepository.getHasRemovedAds();

        if (!hasRemovedAds) {
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(Constants.DEVICE_ID)
                    .build();
            mAdView.loadAd(adRequest);
        }
    }

    private RoundScore calculateScore(FinishRoundData finishRoundData) {

        RoundScore roundScore = new RoundScore(new ArrayList<>(finishRoundData.getPlayedWordsForChallengeId().keySet()));

        boolean levelCompleted = true;

        for(Map.Entry<Integer, String> entry : finishRoundData.getPlayedWordsForChallengeId().entrySet()) {

            Integer challengeId = entry.getKey();

            // TODO: Value here would not be a word but a new type that includes a word as a string and a LevelChallenge with all of its modifiers.
            String word = entry.getValue();

            // TODO: Will need to get the LevelChallengeModifier form the object
            // that GameActivity sends and not from the db
            for (LevelChallengeModifier modifier :
                    this.levelChallengeRepository.getById(challengeId).getLevelChallengeModifiers()) {

                roundScore.addModifier(challengeId, this.loadModifier(modifier));
            }

            roundScore.setPlayedWordForChallengeId(word, challengeId);

            if (this.wordRepository.wordExists(word)) {

                roundScore.setWordExists(challengeId, true);
                boolean meetsAllMandatoryModifiers = true;

                List<IChallengeModifier> challengeModifiers =
                        roundScore.getModifiersForChallengeId(challengeId);

                for (IChallengeModifier modifier : challengeModifiers) {

                    if (modifier.meetsRequirement(word)) {

                        int scoreFromModifier = modifier.getPointsValue();

                        roundScore.setModifierScore(challengeId, modifier, scoreFromModifier);
                    }
                    else {

                        if (modifier.isMandatory()) {

                            roundScore.setZeroChallengeScore(challengeId);
                            levelCompleted = false;
                            meetsAllMandatoryModifiers = false;
                            //break; Let all modifiers be evaluated to have all the score info.
                        }
                        else {
                            roundScore.setZeroModifierScore(challengeId, modifier);
                        }
                    }
                }

                if (meetsAllMandatoryModifiers) {
                    roundScore.setChallengeSuccessScore(challengeId, CHALLENGE_SUCCESS_SCORE);
                }
            }
            else {
                roundScore.setZeroChallengeScore(challengeId);
                roundScore.setWordExists(challengeId, false);
                levelCompleted = false;
            }
        }

        roundScore.setLevelCompleted(levelCompleted);

        if (levelCompleted) {
            roundScore.setTimeScore(finishRoundData.getSecondsLeft() * TIME_SCORE_MULTIPLIER);
        }

        return roundScore;
    }

    private void completeLevel(RoundScore roundScore, FinishRoundData finishRoundData) {

        Level currentLevel = this.levelRepository.getById(finishRoundData.getLevelId());
        Stage currentStage = currentLevel.getStage();



        this.oldScore = currentLevel.getScore();
        this.newScore = this.roundScore.getTotalScore();

        if (roundScore.getLevelCompleted()) {

            if (currentLevel.getIsCompleted()) {

                if (currentLevel.getScore() < roundScore.getTotalScore()) {
                    this.levelRepository.setLevelScore(finishRoundData.getLevelId(), this.roundScore.getTotalScore());
                }
            }
            else {

                this.levelRepository.markLevelAsCompleted(finishRoundData.getLevelId(), this.roundScore.getTotalScore());

                this.levelRepository.unlockNextLevel(currentLevel);

                boolean allLevelsInStageCompleted = true;

                for (Level level : currentStage.getLevels()) {
                    if (!level.getIsCompleted()) {
                        allLevelsInStageCompleted = false;
                    }
                }

                if (allLevelsInStageCompleted) {
                    this.stageRepository.unlockNextStage(currentStage);
                }
            }
        }
        else {
            this.powerUpRepository.decreasePowerUpQuantity(Keys.Products.Skus.Life, Keys.Products.UseQuantity.One);
        }
    }

    private void showRoundResultPreview(RoundScore roundScore) {

        TextView textViewTitle = (TextView) this.findViewById(R.id.textView_Title);
        LinearLayout linearLayoutTitle = (LinearLayout) this.findViewById(R.id.linearLayout_Title);

        ImageButton buttonContinue = (ImageButton) this.findViewById(R.id.button_Continue);

        int currentLives = this.powerUpRepository.getCurrentLives();

        Level currentLevel = this.levelRepository.getById(this.currentLevelId);
        String currentLevelName = currentLevel.getName();

        if (roundScore.getLevelCompleted()) {

            textViewTitle.setText(currentLevelName.toUpperCase() + " COMPLETE");
            buttonContinue.setImageResource(R.drawable.ic_play_arrow_black_48dp);
            linearLayoutTitle.setBackgroundResource(R.drawable.success_status);
        }
        else {

            textViewTitle.setText(currentLevelName.toUpperCase() + " FAILED");
            linearLayoutTitle.setBackgroundResource(R.drawable.failure_status);

            if (currentLives > 0) {
                buttonContinue.setImageResource(R.drawable.ic_replay_black_48dp);
            }
            else {
                this.outOfLives = true;
                buttonContinue.setImageResource(R.drawable.ic_favorite_border_black_48dp);
            }
        }

        textViewLevelScore = (TextView) this.findViewById(R.id.textView_LevelScore);
        textViewLevelScore.setText("Total Score: " + Integer.toString(roundScore.getTotalScore()));

//        finalValue = roundScore.getTotalScore();
//        //finalValue = 20;
//
//        fadeIn = new AlphaAnimation(0.0f, 1.0f);
//        fadeOut = new AlphaAnimation(1.0f, 0.0f);
//
//        fadeIn.setDuration(1000);
//        fadeIn.setFillAfter(true);
//        fadeOut.setDuration(1000);
//        fadeOut.setFillAfter(true);
//
//        fadeIn.setAnimationListener(this);
//        fadeOut.setAnimationListener(this);
//        textViewLevelScore.startAnimation(fadeIn);
        //textViewLevelScore.startAnimation(fadeOut);

        ListView challengesResultsView = (ListView)findViewById(R.id.listView_ChallengeResults);

        final ChallengeResultsListAdapter challengesResultsAdapter = new ChallengeResultsListAdapter(this, this.roundScore);

        challengesResultsView.setAdapter(challengesResultsAdapter);

        challengesResultsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startChallengeResultDetailActivity(
                        position,
                        (int) challengesResultsAdapter.getItemId(position),
                        (ChallengeScore) challengesResultsAdapter.getItem(position));
            }
        });

        Animation titleAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_slow);
        linearLayoutTitle.startAnimation(titleAnimation);
    }

    private IChallengeModifier loadModifier(LevelChallengeModifier challengeModifier) {
        return this.modifierFactory.createModifier(challengeModifier);
    }

    private void submitScore(int score) {
        if(isSignedIn()) {
            Games.Leaderboards.submitScore(getApiClient(), Keys.LEADERBOARD_ID, score);

            Log.i(LOG_TAG, "Score submitted");
        }
        else {
            Log.i(LOG_TAG, "NOT SIGNED IN");
        }
    }

    private void getLeaderboardScore(){
        if(isSignedIn()) {

            Games.Leaderboards.loadCurrentPlayerLeaderboardScore(getApiClient(), Keys.LEADERBOARD_ID, LeaderboardVariant.TIME_SPAN_ALL_TIME, LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
                @Override
                public void onResult(final Leaderboards.LoadPlayerScoreResult scoreResult) {
                    if (isScoreResultValid(scoreResult)) {
                        leaderboardScore = scoreResult.getScore().getRawScore();
                        Log.i(LOG_TAG, "Leaderboard score: " + leaderboardScore);
                    }
                }
            });

            Log.i(LOG_TAG, "Score retrieved");
        }
        else {
            Log.i(LOG_TAG, "NOT SIGNED IN");
        }
    }

    private boolean isScoreResultValid(final Leaderboards.LoadPlayerScoreResult scoreResult) {
        return scoreResult != null && GamesStatusCodes.STATUS_OK == scoreResult.getStatus().getStatusCode() && scoreResult.getScore() != null;
    }

    public void button_Credits_OnClick(View sender){
        startCreditsActivity();
    }

    public void button_Continue_OnClick(View sender) {

        sender.startAnimation(this.scaleButtonAnimation);

        if (this.outOfLives) {
            this.startGetLivesActivity();
        }
        else {
            int levelToLoadId = this.currentLevelId;

            if (this.roundScore.getLevelCompleted()) {
                levelToLoadId = this.levelRepository.getNextLevel(this.currentLevelId).getId();
            }

            this.startGameActivity(levelToLoadId);
        }
    }

    public void button_ShowLeaderboard_OnClick(View sender) {

        sender.startAnimation(this.scaleButtonAnimation);

        this.showLeaderboard();
    }

    public void button_Quit_OnClick(View sender) {

        sender.startAnimation(this.scaleButtonAnimation);

        this.startTitleActivity();
    }

    public void button_ShareScore_OnClick(View view) {

        view.startAnimation(this.scaleButtonAnimation);

        Date now = new Date();
        CharSequence filename = android.text.format.DateFormat.format("WORDWIZ_SCORE_yyyy_MM_dd_hh_mm_ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            screenShotFilePath = Environment.getExternalStorageDirectory().toString() + "/" + filename + ".png";

            // create bitmap screen capture
            Bitmap bitmap = takeScreenShot(this.findViewById(R.id.relativeLayout_ScoreInfo));

            File imageFile = new File(screenShotFilePath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.PNG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            shareImage(screenShotFilePath);

        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    public Bitmap takeScreenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private void shareImage(String imagePath) {
        Intent share = new Intent(Intent.ACTION_SEND);

        // If you want to share a png image only, you can do:
        // setType("image/png"); OR for jpeg: setType("image/jpeg");
        share.setType("image/*");

        File imageFileToShare = new File(imagePath);

        Uri uri = Uri.fromFile(imageFileToShare);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        share.putExtra(Intent.EXTRA_SUBJECT, "My Score");
        share.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_score_message));

        startActivityForResult(Intent.createChooser(share, "Share your score!"), SHARE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SHARE_REQUEST_CODE) {
            File imageFile = new File(screenShotFilePath);
            if (imageFile.exists()) {
                imageFile.delete();
            }
        }
    }

    private void startGetLivesActivity() {
        Intent intent = new Intent(this, GetLivesActivity.class);
        this.startActivity(intent);
    }

    private void startCreditsActivity() {
        Intent intent = new Intent(this, CreditsActivity.class);
        this.startActivity(intent);
    }

    private void startGameActivity(int selectedLevelId) {

        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(Keys.IntentParameters.GameLoadMode, Keys.IntentParameters.GameLoadModes.LevelSelected);
        intent.putExtra(Keys.IntentParameters.LevelId, selectedLevelId);

        this.startActivity(intent);
    }

    private void showLeaderboard() {
        if(isSignedIn()){
            startActivityForResult(
                    Games.Leaderboards.getLeaderboardIntent(this.getApiClient(), Keys.LEADERBOARD_ID),
                    RC_UNUSED);
        }
        else {
            Log.i(LOG_TAG, "NOT SIGNED IN");
        }
    }

    private void startTitleActivity() {
        Intent intent = new Intent(this, TitleActivity.class);

        this.startActivity(intent);
    }

    private void startChallengeResultDetailActivity(int challengePosition, int challengeId, ChallengeScore challengeScore) {
        Intent intent = new Intent(this, ChallengeResultDetailActivity.class);

        intent.putExtra(Keys.IntentParameters.ChallengeIndex, challengePosition);
        intent.putExtra(Keys.IntentParameters.ChallengeId, challengeId);
        intent.putExtra(Keys.IntentParameters.ChallengeScore, challengeScore);

        this.startActivity(intent);
    }

    /* Game Helper related stuff */
    @Override
    public void onSignInFailed() {
        Log.i(LOG_TAG, "FUCK FUCK FUCK FAILED FUCK");
    }

    @Override
    public void onSignInSucceeded() {
        Log.i(LOG_TAG, "FUCK SIGNED IN");

        getLeaderboardScore();
        Log.i(LOG_TAG, "Old score: " + oldScore);
        Log.i(LOG_TAG, "New score: " + newScore);
        
        long totalScore = levelRepository.getTotalScore();
        Log.i(LOG_TAG, "total score: " + totalScore);

        if(roundScore.getLevelCompleted() && newScore > oldScore) {
            this.submitScore((int) totalScore);
        }

        this.achievementsManager.checkAchievements();
    }

    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub

        Log.i("mini", "Count:" + count);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                textViewLevelScore.setText("" + count);
            }
        });

        if (count == finalValue) {
            Log.i(LOG_TAG, "count: " + count + " finalvalue: " + finalValue);
            Log.i(LOG_TAG, "count == finalvalue");
            textViewLevelScore.startAnimation(fadeOut);
            textViewLevelScore.setText("" + finalValue);
        } else {
            ++count;
            textViewLevelScore.startAnimation(fadeIn);
            textViewLevelScore.startAnimation(fadeOut);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public boolean getLevelCompleted() {
        return this.roundScore.getLevelCompleted();
    }

    public int getSecondsLeft() {
        return this.finishRoundData.getSecondsLeft();
    }

    public ArrayList<String> getPlayedWords() {
        return new ArrayList<>(this.finishRoundData.getPlayedWordsForChallengeId().values());
    }

}
