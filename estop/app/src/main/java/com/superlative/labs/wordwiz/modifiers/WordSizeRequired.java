package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by Catherine on 11/25/2015.
 */
public class WordSizeRequired  extends BaseChallengeModifier {

    private String letter;

    public WordSizeRequired(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        int sizeRequired = Integer.parseInt(this.letter);
        return word.length() == sizeRequired;
    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Word size required";
    }

    @Override
    public String toString() {
        return "Length: " + this.letter;
    }
}
