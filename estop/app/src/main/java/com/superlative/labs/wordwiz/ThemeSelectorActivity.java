package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.repositories.ThemeRepository;
import com.superlative.labs.wordwiz.data.model.Theme;


public class ThemeSelectorActivity  extends OrmLiteBaseActivity<DatabaseHelper> {

    private boolean isFirstOnItemSelected;
    ThemeRepository themeRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_selector);
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.themes_layout);
        linearLayout.setBackgroundColor(Color.BLUE);

        TableLayout tableLayout = new TableLayout (this);
        TableLayout.LayoutParams tableLayoutParams = new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        tableLayout.setLayoutParams(tableLayoutParams);
        //layout.setPadding(1, 1, 1, 1);

        this.themeRepository = new ThemeRepository(new DatabaseHelper(this));
        List<Theme> themes = this.themeRepository.getAllThemes();

        int rowCount=0;
        TableRow tr = new TableRow(this);
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(params);

        for(Theme theme : themes){
            if(rowCount == 5){
                tableLayout.addView(tr);
                tr =  new TableRow(this);
                rowCount=0;
            }
            Button btn = new Button (this);
            btn.setId(theme.getId());
            final int id_ = btn.getId();
            btn.setWidth(100);
            btn.setText(theme.getName());
            btn.setTextSize(10.0f);
            btn.setTextColor(Color.rgb(100, 200, 200));

            TableRow.LayoutParams tvPar = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            btn.setLayoutParams(tvPar);
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Toast.makeText(view.getContext(),
                            "Button clicked index = " + id_, Toast.LENGTH_SHORT)
                            .show();

                    Intent intent = new Intent(view.getContext(), GameActivity.class);
                    // TODO: don't use this magic string here. Create a string in the Keys class for this.
                    // Also send a String Extra to Keys.IntentParameters.GameLoadMode.ThemeSelected
                    intent.putExtra("themeid", id_);
                    view.getContext().startActivity(intent);
                }
            });
            tr.addView(btn);
            rowCount++;
        }

        if(rowCount != 5){
            tableLayout.addView(tr);
        }

        linearLayout.addView(tableLayout);

    }

}
