package com.superlative.labs.wordwiz.data.repositories;

import android.util.Log;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Stage;

/**
 * Created by kevin on 9/16/2015.
 */
public class StageRepository {

    private RuntimeExceptionDao<Stage, Integer> dao;

    public StageRepository(DatabaseHelper helper) {
        this.dao = helper.getStageDao();
    }

    public List<Stage> getAllStages() {

        List<Stage> stagesToReturn = this.dao.queryForAll();

        return stagesToReturn;
    }

    public void unlockNextStage(Stage stage) {

        UpdateBuilder<Stage, Integer> updateBuilder = this.dao.updateBuilder();

        try {
            // set the criteria like you would a QueryBuilder
            updateBuilder.where().eq(Stage.ID, stage.getId() + 1);

            // update the value of your field(s)
            updateBuilder.updateColumnValue(Stage.IS_LOCKED, false);

            updateBuilder.update();
        }
        catch (SQLException ex) {
            Log.e("LevelRepository", ex.getMessage());
        }
    }

    public Stage getCurrentStage() {

        try {
            QueryBuilder<Stage, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq(Stage.IS_LOCKED, false);
            queryBuilder.orderBy(Stage.ID, false);

            return this.dao.queryForFirst(queryBuilder.prepare());
        }
        catch (SQLException ex) {
            return null;
        }
    }
}
