package com.superlative.labs.wordwiz.modifiers;

import android.util.Log;

/**
 * Created by Catherine on 11/25/2015.
 */
public class LastVowelRequired  extends BaseChallengeModifier{

    private String letter;

    public LastVowelRequired(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        String lastCharacter = word.substring(word.length() - 1);
        Log.i("LastVowelRequired", "Last character: " + lastCharacter);
        return "aeiou".contains(lastCharacter.toLowerCase());
    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Last letter is a vowel";
    }

    @Override
    public String toString() {
        return "Ends with vowel";
    }
}
