package com.superlative.labs.wordwiz.round;

import java.util.List;

import com.superlative.labs.wordwiz.data.model.Word;

public class RoundData 
{	
	public String currentLetter;
	public List<Word> playedWords;
	public int secondsLeft;
	
	public RoundData(String currentLetter, List<Word> playedWords, int secondsLeft) {
		this.currentLetter = currentLetter;
		this.playedWords = playedWords;
		this.secondsLeft = secondsLeft;
	}
}
