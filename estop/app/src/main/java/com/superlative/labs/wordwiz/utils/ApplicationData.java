package com.superlative.labs.wordwiz.utils;

import android.app.Application;

/**
 * Created by kevin on 1/27/2016.
 */
public class ApplicationData extends Application {

    private boolean useTimePlusPowerUp = false;

    public boolean getUseTimePlusPowerUp() { return this.useTimePlusPowerUp; }
    public void setUseTimePlusPowerUp(boolean value) { this.useTimePlusPowerUp = value; }

    public void resetUseTimePlusPowerUp() {
        this.useTimePlusPowerUp = false;
    }
}
