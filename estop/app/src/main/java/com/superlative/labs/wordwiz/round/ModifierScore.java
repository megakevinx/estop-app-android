package com.superlative.labs.wordwiz.round;

import com.superlative.labs.wordwiz.modifiers.IChallengeModifier;

/**
 * Created by kevin on 10/4/2015.
 */
public class ModifierScore {

    private int score;
    private IChallengeModifier challengeModifier;

    public ModifierScore(int score, IChallengeModifier challengeModifier) {
        this.score = score;
        this.challengeModifier = challengeModifier;
    }

    public int getScore() {
        return score;
    }

    public IChallengeModifier getChallengeModifier() {
        return challengeModifier;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
