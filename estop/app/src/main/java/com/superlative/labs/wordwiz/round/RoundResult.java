package com.superlative.labs.wordwiz.round;

public class RoundResult
{
    public Boolean isFinished;
    public RoundScore playerScore;
    public RoundScore opponentScore;
    public GameResult result;
}