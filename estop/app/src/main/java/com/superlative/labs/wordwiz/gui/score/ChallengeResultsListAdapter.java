package com.superlative.labs.wordwiz.gui.score;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.superlative.labs.wordwiz.R;

import com.superlative.labs.wordwiz.round.ChallengeScore;
import com.superlative.labs.wordwiz.round.RoundScore;

/**
 * Created by kevin on 11/9/2015.
 */
public class ChallengeResultsListAdapter extends BaseAdapter implements ListAdapter {

    private final Context context;
    private final RoundScore roundScore;

    private static int MAX_WORD_LENGTH_TO_SHOW = 10;

    public ChallengeResultsListAdapter(Context context, RoundScore roundScore) {
        this.context = context;
        this.roundScore = roundScore;
    }

    @Override
    public int getCount() {
        return this.roundScore.getScoresByChallengeId().size();
    }

    @Override
    public Object getItem(int position) {
        return (ChallengeScore) this.roundScore.getScoresByChallengeId().values().toArray()[position];
    }

    @Override
    public long getItemId(int position) {
        return (Integer) this.roundScore.getScoresByChallengeId().keySet().toArray()[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ChallengeScore challenge = (ChallengeScore) this.getItem(position);
        int layoutToLoad;

        if (challenge.getScore() > 0) {
            layoutToLoad = R.layout.list_item_challenge_success;
        }
        else {
            layoutToLoad = R.layout.list_item_challenge_failure;
        }

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutToLoad, parent, false);
        }

        TextView textViewChallengeIndex = (TextView) convertView.findViewById(R.id.textView_ChallengeIndex);
        TextView textViewChallengeScore = (TextView) convertView.findViewById(R.id.textView_ChallengeScore);

        String wordToShow = challenge.getPlayedWord();

        if (wordToShow.length() > MAX_WORD_LENGTH_TO_SHOW) {
            wordToShow = wordToShow.substring(0, MAX_WORD_LENGTH_TO_SHOW) + "...";
        }
        else if (wordToShow.length() == 0) {
            wordToShow = "No word played";
        }

        textViewChallengeIndex.setText(wordToShow);
        textViewChallengeScore.setText(Integer.toString(challenge.getScore()));

        return convertView;
    }
}
