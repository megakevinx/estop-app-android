package com.superlative.labs.wordwiz.gui.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import com.superlative.labs.wordwiz.R;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.utils.billing.SkuDetails;

/**
 * Created by kevin on 11/5/2015.
 */
public class InAppProductsListAdapter extends BaseAdapter implements ListAdapter {

    Context context;
    List<SkuDetails> productsForSale;
    private static String appName = " (Word Wiz)";

    public InAppProductsListAdapter(Context context, List<SkuDetails> productsForSale) {
        this.context = context;
        this.productsForSale = productsForSale;
    }

    @Override
    public int getCount() {
        return this.productsForSale.size();
    }

    @Override
    public Object getItem(int position) {
        return this.productsForSale.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_product, parent, false);
        }

        SkuDetails product = ((SkuDetails) this.getItem(position));
        String sku = product.getSku();
        int productIconResourceId = R.drawable.product_clock_10;

        if (sku.equals(Keys.Products.Skus.TimePlus)) {
            productIconResourceId = R.drawable.ic_alarm_add_black_48dp;
        }
        else if (sku.equals(Keys.Products.Skus.Life)) {
            productIconResourceId = R.drawable.ic_favorite_border_black_48dp;
        }
        else if (sku.equals(Keys.Products.Skus.RemoveAds)) {
            productIconResourceId = R.drawable.ic_stars_black_48dp;
        }

        String title = product.getTitle().replace(InAppProductsListAdapter.appName, "");
        String price = product.getPrice();

        ImageView imageViewIcon = (ImageView) convertView.findViewById(R.id.imageView_ProductIcon);
        TextView textViewTitle = (TextView) convertView.findViewById(R.id.textView_ProductTitle);
        TextView textViewPrice = (TextView) convertView.findViewById(R.id.textView_ProductPrice);

        imageViewIcon.setImageResource(productIconResourceId);
        textViewTitle.setText(title);
        textViewPrice.setText(price + " USD");

        return convertView;
    }
}
