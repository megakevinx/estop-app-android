package com.superlative.labs.wordwiz.round;

import android.content.Context;

/**
 * Created by Catherine on 8/28/2015.
 */
public class AccomplishmentsOutbox {

    boolean mPrimeAchievement = false;
    boolean mHumbleAchievement = false;
    boolean mLeetAchievement = false;
    boolean mArrogantAchievement = false;
    int mBoredSteps = 0;
    int mEasyModeScore = -1;
    int mHardModeScore = -1;

    boolean isEmpty() {
        return !mPrimeAchievement && !mHumbleAchievement && !mLeetAchievement &&
                !mArrogantAchievement && mBoredSteps == 0 && mEasyModeScore < 0 &&
                mHardModeScore < 0;
    }

    public void saveLocal(Context ctx) {
            /* TODO: This is left as an exercise. To make it more difficult to cheat,
             * this data should be stored in an encrypted file! And remember not to
             * expose your encryption key (obfuscate it by building it from bits and
             * pieces and/or XORing with another string, for instance). */
    }

    public void loadLocal(Context ctx) {
            /* TODO: This is left as an exercise. Write code here that loads data
             * from the file you wrote in saveLocal(). */
    }
}
