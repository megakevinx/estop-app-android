package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class UnlockGameActivity extends BaseGameActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unlock_game);
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    public void button_PurchaseUnlockGame_OnClick(View sender) {
        this.startStoreActivity();
    }

    private void startStoreActivity() {
        Intent intent = new Intent(this, StoreActivity.class);

        intent.putExtra(Keys.IntentParameters.AutoBuy, true);
        intent.putExtra(Keys.IntentParameters.AutoBuySku, Keys.Products.Skus.UnlockGame);

        this.startActivity(intent);
    }
}
