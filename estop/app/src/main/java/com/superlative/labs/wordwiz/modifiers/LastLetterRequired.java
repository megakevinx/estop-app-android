package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by Catherine on 11/26/2015.
 */
public class LastLetterRequired extends BaseChallengeModifier {

    private String letter;

    public LastLetterRequired(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        String lastCharacter = word.substring(word.length() - 1);
        return lastCharacter.toLowerCase().equals(letter.toLowerCase());
    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Last letter is";
    }

    @Override
    public String toString() {
        return "Ends with: " + this.letter.toUpperCase();
    }
}
