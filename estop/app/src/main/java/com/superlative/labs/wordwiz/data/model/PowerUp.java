package com.superlative.labs.wordwiz.data.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by kevin on 11/7/2015.
 */
public class PowerUp {

    @DatabaseField(generatedId = true)
    int id;

    public static final String SKU = "sku";
    @DatabaseField(canBeNull = false, columnName = SKU, unique = true)
    String sku;

    public static final String QUANTITY = "quantity";
    @DatabaseField(canBeNull = false, columnName = QUANTITY)
    int quantity;

    PowerUp() { }

    public PowerUp(String sku, int quantity) {
        this.sku = sku;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }
}
