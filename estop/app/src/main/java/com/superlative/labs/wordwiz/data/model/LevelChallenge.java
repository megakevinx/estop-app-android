package com.superlative.labs.wordwiz.data.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by kevin on 8/30/2015.
 */
@DatabaseTable
public class LevelChallenge {
    @DatabaseField(id = true)
    int id;

    @DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh=true)
    Level level;

    @ForeignCollectionField
    ForeignCollection<LevelChallengeModifier> levelChallengeModifiers;

    LevelChallenge() { }

    public LevelChallenge(int id, Level level) {
        this.id = id;
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public Level getLevel() {
        return level;
    }

    public Collection<LevelChallengeModifier> getLevelChallengeModifiers() {
        return levelChallengeModifiers;
    }
}

