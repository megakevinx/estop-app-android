package com.superlative.labs.wordwiz.gui.challenge;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superlative.labs.wordwiz.R;
import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.LevelChallenge;
import com.superlative.labs.wordwiz.data.model.LevelChallengeModifier;
import com.superlative.labs.wordwiz.data.repositories.LevelChallengeRepository;
import com.superlative.labs.wordwiz.modifiers.IChallengeModifier;
import com.superlative.labs.wordwiz.modifiers.ModifierFactory;

public class ChallengeWordFragment extends Fragment {

    public static final String ARG_CHALLENGE = "challenge";
    private static final int MAX_WORD_LENGTH = 18;
    private static final int FIRST_ROW_LETTER_LIMIT = 6;
    private static final int SECOND_ROW_LETTER_LIMIT = 12;

    IChallengeWordListener listener;
    LevelChallengeRepository levelChallengeRepository;

    ModifierFactory modifierFactory;

    //TextView challengeWord;
    int challengeId;
    String challengeWord;

    View rootView;
    //private Animation letterAddAnimation;
    //private Animation letterRemoveAnimation;

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        this.listener = (IChallengeWordListener) context;

        this.levelChallengeRepository = new LevelChallengeRepository(new DatabaseHelper(context));

        this.modifierFactory = new ModifierFactory();

//        this.letterAddAnimation = AnimationUtils.loadAnimation(context, R.anim.scale_up);
        //this.letterRemoveAnimation = AnimationUtils.loadAnimation(context, R.anim.scale_down);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.rootView = inflater.inflate(R.layout.fragment_challenge_word, container, false);

        Bundle args = getArguments();
        this.challengeId = args.getInt(ARG_CHALLENGE);

        this.loadLevelChallenge(this.rootView, challengeId);

        this.challengeWord = "";

        return rootView;
    }

    public void setChallengeWord(String newWord) {

        if (newWord.length() <= MAX_WORD_LENGTH) {

            LinearLayout challengeWordLetterContainerRow1 = (LinearLayout) this.rootView.findViewById(R.id.linearLayout_ChallengeWordRow1);
            LinearLayout challengeWordLetterContainerRow2 = (LinearLayout) this.rootView.findViewById(R.id.linearLayout_ChallengeWordRow2);
            LinearLayout challengeWordLetterContainerRow3 = (LinearLayout) this.rootView.findViewById(R.id.linearLayout_ChallengeWordRow3);

            if (this.challengeWord.length() > newWord.length()) {

                Animation letterRemoveAnimation = AnimationUtils.loadAnimation(this.getContext(), R.anim.scale_down);
                letterRemoveAnimation.setFillAfter(true);

                final View letter;
                final LinearLayout parent;

                if (this.challengeWord.length() <= FIRST_ROW_LETTER_LIMIT) {
                    letter = challengeWordLetterContainerRow1.getChildAt(this.challengeWord.length() - 1);
                    parent = challengeWordLetterContainerRow1;
                }
                else if (this.challengeWord.length() > FIRST_ROW_LETTER_LIMIT && this.challengeWord.length() <= SECOND_ROW_LETTER_LIMIT) {
                    letter = challengeWordLetterContainerRow2.getChildAt(this.challengeWord.length() - FIRST_ROW_LETTER_LIMIT - 1);
                    parent = challengeWordLetterContainerRow2;
                }
                else {
                    letter = challengeWordLetterContainerRow3.getChildAt(this.challengeWord.length() - SECOND_ROW_LETTER_LIMIT - 1);
                    parent = challengeWordLetterContainerRow3;
                }

                letterRemoveAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) { }
                    @Override
                    public void onAnimationRepeat(Animation animation) { }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        //parent.removeView(letter);

                        Runnable dirtyHack = new Runnable() {
                            @Override
                            public void run() {
                                parent.removeView(letter);
                            }
                        };
                        Handler handler = new Handler();
                        handler.postDelayed(dirtyHack, 100);
                    }
                });

                letter.startAnimation(letterRemoveAnimation);
            }
            else if (this.challengeWord.length() < newWord.length()) {

                Animation letterAddAnimation = AnimationUtils.loadAnimation(this.getContext(), R.anim.scale_up);

                TextView playedLetter;

                if (Build.VERSION.SDK_INT < 21) {
                    playedLetter = new TextView(this.getContext());
                    playedLetter.setBackgroundResource(R.drawable.played_letter);
                    playedLetter.setPadding(20, 15, 20, 15);
                    playedLetter.setTextSize(30);
                }
                else {
                    playedLetter = new TextView(this.getContext(), null, 0, R.style.played_letter);
                }

                playedLetter.setText(Character.toString(newWord.toCharArray()[newWord.length() - 1]));

                LinearLayout.LayoutParams layoutParams =
                        new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);

                layoutParams.setMargins(2, 0, 2, 0);

                if (newWord.length() - 1 < FIRST_ROW_LETTER_LIMIT) {
                    challengeWordLetterContainerRow1.addView(playedLetter, layoutParams);
                } else if (newWord.length() - 1 >= FIRST_ROW_LETTER_LIMIT && newWord.length() - 1 < SECOND_ROW_LETTER_LIMIT) {
                    challengeWordLetterContainerRow2.addView(playedLetter, layoutParams);
                } else {
                    challengeWordLetterContainerRow3.addView(playedLetter, layoutParams);
                }

                playedLetter.startAnimation(letterAddAnimation);
            }

            this.challengeWord = newWord;
            notifyAfterTextChanged(this.challengeWord, challengeId);
        }
    }

    public String getChallengeWord() {
        return this.challengeWord;
    }

    private void loadLevelChallenge(View rootView, int challengeId) {

        LinearLayout modifiersContainer = (LinearLayout) rootView.findViewById(R.id.linearLayout_ChallengeModifiers);
        LevelChallenge challenge = this.levelChallengeRepository.getById(challengeId);

        for (LevelChallengeModifier challengeModifier : challenge.getLevelChallengeModifiers()) {

            IChallengeModifier modifier = this.modifierFactory.createModifier(challengeModifier);

            TextView textViewModifier;

            if (Build.VERSION.SDK_INT < 21) {
                textViewModifier = new TextView(this.getContext());
                textViewModifier.setBackgroundResource(R.drawable.challenge_modifier);
                textViewModifier.setPadding(15, 10, 15, 10);
                textViewModifier.setTextSize(15);
            }
            else {
                textViewModifier = new TextView(this.getContext(), null, 0, R.style.challenge_modifier);
            }

            textViewModifier.setText(modifier.toString());

            LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(5, 0, 5, 0);

            modifiersContainer.addView(textViewModifier, layoutParams);
        }
    }

    private void notifyAfterTextChanged(String text, int challengeId) {
        this.listener.notifyChallengeWordTextChanged(text, challengeId);
    }
}