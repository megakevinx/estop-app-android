package com.superlative.labs.wordwiz.utils.achievements;

/**
 * Created by kevin on 2/8/2016.
 */
public interface IAchievementApiClient {
    void unlock(String achievementCode);
}
