package com.superlative.labs.wordwiz.round;

public enum GameResult 
{
    Win, Lose, Tie
}