package com.superlative.labs.wordwiz.data.loaders;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.superlative.labs.wordwiz.data.RealDatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Category;
import com.superlative.labs.wordwiz.data.model.Level;
import com.superlative.labs.wordwiz.data.model.LevelChallenge;
import com.superlative.labs.wordwiz.data.model.LevelChallengeModifier;
import com.superlative.labs.wordwiz.data.model.Modifier;
import com.superlative.labs.wordwiz.data.model.Stage;
import com.superlative.labs.wordwiz.data.model.Theme;
import com.superlative.labs.wordwiz.data.model.Word;

/**
 * Created by kevin on 8/30/2015.
 */
public class DataLoader {

    RealDatabaseHelper databaseHelper;

    public DataLoader(RealDatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public void loadThemeData(String dataFile) throws IOException {
        ArrayList<String[]> themes = this.readCsv(this.databaseHelper.getContext().getAssets().open(dataFile));

        RuntimeExceptionDao<Theme, Integer> themeDao = this.databaseHelper.getThemeDao();

        for (String[] dataRow : themes) {

            int id = Integer.parseInt(dataRow[0]);
            String name = dataRow[1];
            String description = dataRow[2];

            themeDao.create(new Theme(id, name, description));
        }
    }

    public void loadCategoryData(String dataFile) throws IOException {
        ArrayList<String[]> categories = this.readCsv(this.databaseHelper.getContext().getAssets().open(dataFile));

        RuntimeExceptionDao<Category, Integer> categoryDao = this.databaseHelper.getCategoryDao();
        RuntimeExceptionDao<Theme, Integer> themeDao = this.databaseHelper.getThemeDao();

        for (String[] dataRow : categories) {

            int id = Integer.parseInt(dataRow[0]);
            String name = dataRow[1];
            Theme theme = themeDao.queryForId(Integer.parseInt(dataRow[2]));

            categoryDao.create(new Category(id, name, theme));
        }
    }

    private List<String> oneLetterWords = Arrays.asList("A", "I", "O");

    public void loadWordData(String dataFile) throws IOException {
        ArrayList<String[]> words = this.readCsv(this.databaseHelper.getContext().getAssets().open(dataFile));

        RuntimeExceptionDao<Word, Integer> wordDao = this.databaseHelper.getWordDao();
        RuntimeExceptionDao<Category, Integer> categoryDao = this.databaseHelper.getCategoryDao();

        for (String[] dataRow : words) {

            int id = Integer.parseInt(dataRow[0]);
            String name = dataRow[1].toLowerCase();
            Category category = categoryDao.queryForId(Integer.parseInt(dataRow[2]));

            if (name.length() == 1 && !oneLetterWords.contains(name.toUpperCase()))
                continue;

            wordDao.create(new Word(id, name, category));
        }
    }

    public void loadStageData(String dataFile) throws IOException {

        ArrayList<String[]> stages = this.readCsv(this.databaseHelper.getContext().getAssets().open(dataFile));

        RuntimeExceptionDao<Stage, Integer> stageDao = this.databaseHelper.getStageDao();

        for (String[] dataRow : stages) {

            int id = Integer.parseInt(dataRow[0]);
            int sequence = Integer.parseInt(dataRow[1]);
            String name = dataRow[2];
            boolean isLocked  = Boolean.parseBoolean(dataRow[3]);

            stageDao.create(new Stage(id, sequence, name, isLocked));
        }
    }

    public void loadLevelData(String dataFile) throws IOException {

        ArrayList<String[]> levels = this.readCsv(this.databaseHelper.getContext().getAssets().open(dataFile));

        RuntimeExceptionDao<Level, Integer> levelDao = this.databaseHelper.getLevelDao();
        RuntimeExceptionDao<Stage, Integer> stageDao = this.databaseHelper.getStageDao();

        boolean isFirstLevel = true;

        for (String[] dataRow : levels) {

            int id = Integer.parseInt(dataRow[0]);
            int sequence = Integer.parseInt(dataRow[1]);
            Stage stage = stageDao.queryForId(Integer.parseInt(dataRow[2]));
            String name = dataRow[3];
            int seconds = Integer.parseInt(dataRow[4]);
            int score = 0;
            boolean isCompleted = false;
            boolean isLocked = true;

            if (isFirstLevel)
            {
                isLocked = false;
                isFirstLevel = false;
            }

            levelDao.create(new Level(id, sequence, stage, name, seconds, score, isCompleted, isLocked));
        }
    }

    public void loadLevelChallengeData(String dataFile) throws IOException {

        ArrayList<String[]> levelChallenges = this.readCsv(this.databaseHelper.getContext().getAssets().open(dataFile));

        RuntimeExceptionDao<LevelChallenge, Integer> levelChallengeDao = this.databaseHelper.getLevelChallengeDao();
        RuntimeExceptionDao<Level, Integer> levelDao = this.databaseHelper.getLevelDao();

        for (String[] dataRow : levelChallenges) {

            int id = Integer.parseInt(dataRow[0]);
            Level level = levelDao.queryForId(Integer.parseInt(dataRow[1]));

            levelChallengeDao.create(new LevelChallenge(id, level));
        }
    }

    public void loadLevelChallengeModifierData(String dataFile) throws IOException {

        ArrayList<String[]> levelChallengeModifiers = this.readCsv(this.databaseHelper.getContext().getAssets().open(dataFile));

        RuntimeExceptionDao<LevelChallengeModifier, Integer> levelChallengeModifierDao = this.databaseHelper.getLevelChallengeModifierDao();
        RuntimeExceptionDao<LevelChallenge, Integer> levelChallengeDao = this.databaseHelper.getLevelChallengeDao();
        RuntimeExceptionDao<Category, Integer> categoryDao = this.databaseHelper.getCategoryDao();

        for (String[] dataRow : levelChallengeModifiers) {

            int id = Integer.parseInt(dataRow[0]);
            LevelChallenge levelChallenge = levelChallengeDao.queryForId(Integer.parseInt(dataRow[1]));
            Modifier modifier = Modifier.valueOf(dataRow[2]);
            String letter = dataRow[3];
            Category category = categoryDao.queryForId(Integer.parseInt(dataRow[4]));
            int value = Integer.parseInt(dataRow[5]);

            levelChallengeModifierDao.create(
                    new LevelChallengeModifier(id, levelChallenge, modifier, letter, category, value));
        }
    }

    public ArrayList<String[]> readCsv(InputStream inputStream) {
        ArrayList<String[]> resultList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(",");
                resultList.add(row);
            }
        }
        catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        finally {
            try {
                inputStream.close();
            }
            catch (IOException e) {
                throw new RuntimeException("Error while closing input stream: " + e);
            }
        }
        return resultList;
    }
}
