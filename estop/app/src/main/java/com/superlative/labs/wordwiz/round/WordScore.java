package com.superlative.labs.wordwiz.round;

public class WordScore
{
    public String category;
    public String word;
    public int scorePoints; // es esto necesario? La puntuacion podria calcularse con el RelativeScore
    public RelativeScore relativeScore;
}