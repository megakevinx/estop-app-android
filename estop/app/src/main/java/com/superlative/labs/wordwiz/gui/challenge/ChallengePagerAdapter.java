package com.superlative.labs.wordwiz.gui.challenge;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.HashMap;
import java.util.List;

// Since this is an object collection, use a FragmentStatePagerAdapter,
// and NOT a FragmentPagerAdapter.
public class ChallengePagerAdapter extends FragmentPagerAdapter {

    List<Integer> challengesIds;

    HashMap<Integer, ChallengeWordFragment> wordFragmentList;

    public ChallengePagerAdapter(FragmentManager fm, List<Integer> challengesIds) {
        super(fm);

        this.challengesIds = challengesIds;
        wordFragmentList = new HashMap<>();
    }

    @Override
    public Fragment getItem(int i) {
        ChallengeWordFragment fragment = new ChallengeWordFragment();

        wordFragmentList.put(i, fragment);

        Bundle args = new Bundle();
        args.putInt(ChallengeWordFragment.ARG_CHALLENGE, this.challengesIds.get(i));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public long getItemId(int index) {
        return this.challengesIds.get(index);
    }

    public HashMap<Integer, ChallengeWordFragment> getWordFragmentList(){
        return wordFragmentList;
    }

    @Override
    public int getCount() {
        return this.challengesIds.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Challenge #" + Integer.toString(position + 1);
    }
}