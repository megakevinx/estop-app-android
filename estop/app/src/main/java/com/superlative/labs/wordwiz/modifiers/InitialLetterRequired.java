package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by kevin on 8/18/2015.
 */
public class InitialLetterRequired extends BaseChallengeModifier {

    private String letter;

    public InitialLetterRequired(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        return word.toLowerCase().startsWith(this.letter.toLowerCase());
    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Initial Letter";
    }

    @Override
    public String toString() {
        return "Starts with: " + this.letter.toUpperCase();
    }
}
