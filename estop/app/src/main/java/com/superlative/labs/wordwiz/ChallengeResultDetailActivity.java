package com.superlative.labs.wordwiz;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collection;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.LevelChallengeModifier;
import com.superlative.labs.wordwiz.data.repositories.LevelChallengeRepository;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.gui.score.ChallengeModifiersResultsListAdapter;
import com.superlative.labs.wordwiz.modifiers.IChallengeModifier;
import com.superlative.labs.wordwiz.modifiers.ModifierFactory;
import com.superlative.labs.wordwiz.round.ChallengeScore;
import com.superlative.labs.wordwiz.round.ModifierScore;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class ChallengeResultDetailActivity extends BaseGameActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_result_detail);

        int selectedChallengeIndex = this.getIntent().getIntExtra(Keys.IntentParameters.ChallengeIndex, 1);
        int selectedChallengeId = this.getIntent().getIntExtra(Keys.IntentParameters.ChallengeId, 1);
        ChallengeScore selectedChallengeScore = this.getIntent().getParcelableExtra(Keys.IntentParameters.ChallengeScore);

        this.loadFullChallengeModifiers(selectedChallengeId, selectedChallengeScore);

        this.displayChallengeResultDetail(selectedChallengeIndex, selectedChallengeScore);
    }

    private void loadFullChallengeModifiers(int selectedChallengeId, ChallengeScore selectedChallengeScore) {

        ModifierFactory modifierFactory = new ModifierFactory();

        LevelChallengeRepository levelChallengeRepository = new LevelChallengeRepository(new DatabaseHelper(this));
        Collection<LevelChallengeModifier> levelChallengeModifiers = levelChallengeRepository.getById(selectedChallengeId).getLevelChallengeModifiers();

        for (LevelChallengeModifier modifier : levelChallengeModifiers) {

            int modifierId = modifier.getId();

            IChallengeModifier challengeModifier = modifierFactory.createModifier(modifier);
            int score = selectedChallengeScore.getScoresByModifierId().get(modifierId).getScore();

            ModifierScore newModifierScore = new ModifierScore(score, challengeModifier);

            selectedChallengeScore.getScoresByModifierId().put(modifierId, newModifierScore);
        }
    }

    private void displayChallengeResultDetail(int selectedChallengeIndex, ChallengeScore selectedChallengeScore) {


        RelativeLayout relativeLayoutWordExists = (RelativeLayout) this.findViewById(R.id.relativeLayout_WordExists);
        ImageView imageViewLevelComplete = (ImageView) this.findViewById(R.id.imageView_LevelComplete);
        TextView textViewModifierWordExists = (TextView) this.findViewById(R.id.textView_ModifierWordExists);


        if (selectedChallengeScore.getWordExists()) {
            relativeLayoutWordExists.setBackgroundResource(R.drawable.success_status);
            imageViewLevelComplete.setImageResource(R.drawable.ic_check_black_48dp);
            textViewModifierWordExists.setText("Word Exists");
        }
        else {
            relativeLayoutWordExists.setBackgroundResource(R.drawable.failure_status);
            imageViewLevelComplete.setImageResource(R.drawable.ic_close_black_48dp);
            textViewModifierWordExists.setText("Word Does Not Exist");
        }

        TextView textViewTitle = (TextView) this.findViewById(R.id.textView_Title);
        textViewTitle.setText("Challenge #" + Integer.toString(selectedChallengeIndex + 1));

        TextView textViewPlayedWord = (TextView) this.findViewById(R.id.textView_Message);
        textViewPlayedWord.setText(selectedChallengeScore.getPlayedWord());

        ListView listViewChallengeModifiersResults = (ListView)findViewById(R.id.listView_ChallengeModifiersResults);

        ChallengeModifiersResultsListAdapter challengesModifiersResultsAdapter =
                new ChallengeModifiersResultsListAdapter(this, selectedChallengeScore.getScoresByModifierId());

        listViewChallengeModifiersResults.setAdapter(challengesModifiersResultsAdapter);
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }
}
