package com.superlative.labs.wordwiz.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Catherine on 6/23/2015.
 */
@DatabaseTable
public class Word implements Parcelable {

    @DatabaseField(id = true)
    int id;

    @DatabaseField(index = true)
    String name;
    public static final String NAME = "name";

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    Category category;
    public static final String CATEGORY_ID = "category_id";

    Word() { }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Category getCategory() {
        return category;
    }

    public Word(int id, String name, Category category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    // 99.9% of the time you can just ignore this
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(this.id);
        out.writeString(this.name);
        out.writeString(this.category.getName());
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Word> CREATOR = new Parcelable.Creator<Word>() {
        public Word createFromParcel(Parcel in) {
            return new Word(in);
        }

        public Word[] newArray(int size) {
            return new Word[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Word(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.category = new Category(1, in.readString(), new Theme(100, "fuck", "wtf"));
    }
}
