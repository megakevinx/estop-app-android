package com.superlative.labs.wordwiz.data.repositories;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Category;
import com.superlative.labs.wordwiz.data.model.Word;

public class WordRepository {

    private RuntimeExceptionDao<Word, Integer> dao;

    public WordRepository(DatabaseHelper helper) {
        this.dao = helper.getWordDao();
    }

    public boolean wordExists(String word) {
        List<Word> matchingWords = this.dao.queryForEq(Word.NAME, word.toLowerCase());

        return matchingWords != null &&  !matchingWords.isEmpty();
    }

    public boolean wordIsInCategory(String word, Category category) {

        try {
            QueryBuilder<Word, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq(Word.CATEGORY_ID, category.getId()).and().eq(Word.NAME, word.toLowerCase());

            List<Word> matchingWords = this.dao.query(queryBuilder.prepare());

            return !matchingWords.isEmpty();
        }
        catch (SQLException ex) {
            return false;
        }
    }
}
