package com.superlative.labs.wordwiz.gui.stage;

import java.util.HashMap;

import com.superlative.labs.wordwiz.data.model.Stage;

import com.superlative.labs.wordwiz.R;

/**
 * Created by kevin on 3/6/2016.
 */
public class StageIconProvider {

    private HashMap<Integer, Integer> stageIdToIconResourceIdMap;

    public StageIconProvider() {
        stageIdToIconResourceIdMap = new HashMap<>();

        stageIdToIconResourceIdMap.put(1, R.drawable.ic_stage_apprentice);
        stageIdToIconResourceIdMap.put(2, R.drawable.ic_stage_alchemist);
        stageIdToIconResourceIdMap.put(3, R.drawable.ic_stage_fairy);
        stageIdToIconResourceIdMap.put(4, R.drawable.ic_stage_fortuneteller);
        stageIdToIconResourceIdMap.put(5, R.drawable.ic_stage_seer);
        stageIdToIconResourceIdMap.put(6, R.drawable.ic_stage_prophet);
        stageIdToIconResourceIdMap.put(7, R.drawable.ic_stage_shaman);
        stageIdToIconResourceIdMap.put(8, R.drawable.ic_stage_necromancer);
        stageIdToIconResourceIdMap.put(9, R.drawable.ic_stage_conjurer);
        stageIdToIconResourceIdMap.put(10, R.drawable.ic_stage_invoker);
        stageIdToIconResourceIdMap.put(11, R.drawable.ic_stage_summoner);
        stageIdToIconResourceIdMap.put(12, R.drawable.ic_stage_enchanter);
        stageIdToIconResourceIdMap.put(13, R.drawable.ic_stage_druid);
        stageIdToIconResourceIdMap.put(14, R.drawable.ic_stage_elementalist);
        stageIdToIconResourceIdMap.put(15, R.drawable.ic_stage_animist);
        stageIdToIconResourceIdMap.put(16, R.drawable.ic_stage_mentalist);
        stageIdToIconResourceIdMap.put(17, R.drawable.ic_stage_mystic);
        stageIdToIconResourceIdMap.put(18, R.drawable.ic_stage_spellcaster);
        stageIdToIconResourceIdMap.put(19, R.drawable.ic_stage_magician);
        stageIdToIconResourceIdMap.put(20, R.drawable.ic_stage_witch);
        stageIdToIconResourceIdMap.put(21, R.drawable.ic_stage_sorcerer);
        stageIdToIconResourceIdMap.put(22, R.drawable.ic_stage_mage);
        stageIdToIconResourceIdMap.put(23, R.drawable.ic_stage_archmage);
        stageIdToIconResourceIdMap.put(24, R.drawable.ic_stage_sage);
        stageIdToIconResourceIdMap.put(25, R.drawable.ic_stage_warlock);

    }

    public int getIconResourceId(Stage stage) {
        if (this.stageIdToIconResourceIdMap.containsKey(stage.getId())) {
            return stageIdToIconResourceIdMap.get(stage.getId());
        }

        return R.drawable.ic_stage_apprentice;
    }

}
