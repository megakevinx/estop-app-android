package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by kevin on 8/18/2015.
 */
public interface IChallengeModifier {

    int getPointsValue();
    boolean meetsRequirement(String word);
    int getId();
    boolean isMandatory();
    String getDescription();
}
