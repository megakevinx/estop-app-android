package com.superlative.labs.wordwiz.data.model;

/**
 * Created by kevin on 8/30/2015.
 */
public enum Modifier {
    CategoryRequired,
    DoubleLetterRequired,
    InitialLetterRequired,
    LetterProhibited,
    LetterRequired,
    LetterUseBonus,
    LetterUsePenalty,
    LastVowelRequired,
    InitialVowelRequired,
    MiddleLetterRequired,
    Palindrome,
    VowelNumberRequired,
    WordSizeRequired,
    LastLetterRequired
}
