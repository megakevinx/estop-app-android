package com.superlative.labs.wordwiz.gui.level;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import com.superlative.labs.wordwiz.R;
import com.superlative.labs.wordwiz.data.model.Level;

/**
 * Created by kevin on 11/11/2015.
 */
public class LevelsListAdapter extends BaseAdapter implements ListAdapter {

    private final List<Level> levels;
    private final Context context;

    private final LevelIconProvider levelIconProvider;

    public LevelsListAdapter(Context context, List<Level> levels) {
        this.context = context;
        this.levels = levels;

        this.levelIconProvider = new LevelIconProvider();
    }

    @Override
    public int getCount() {
        return this.levels.size();
    }

    @Override
    public Object getItem(int position) {
        return this.levels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.levels.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Level level = ((Level) this.getItem(position));
        int layoutToLoad;

        if (level.getIsCompleted()) {
            layoutToLoad = R.layout.list_item_level_completed;
        }
        else if (level.getIsLocked()) {
            layoutToLoad = R.layout.list_item_level_locked;
        }
        else {
            layoutToLoad = R.layout.list_item_level_not_completed;
        }

        //if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutToLoad, parent, false);
        //}

        TextView textViewName = (TextView) convertView.findViewById(R.id.textView_Name);
        TextView textViewScore = (TextView) convertView.findViewById(R.id.textView_Score);
        ImageView imageViewIcon = (ImageView) convertView.findViewById(R.id.imageView_Icon);

        String name = level.getName();
        String score = Integer.toString(level.getScore());
        Integer iconResourceId = this.levelIconProvider.getIconResourceId(level);

        textViewName.setText(name);
        textViewScore.setText(score);
        imageViewIcon.setImageResource(iconResourceId);

        return convertView;
    }
}