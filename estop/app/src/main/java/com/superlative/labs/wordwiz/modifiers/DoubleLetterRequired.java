package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by Catherine on 11/25/2015.
 */
public class DoubleLetterRequired extends BaseChallengeModifier  {

    private String letter;

    public DoubleLetterRequired(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        String doubleLetter = this.letter.trim() + this.letter.trim();
        return word.contains(doubleLetter);
    }

    @Override
    public boolean isMandatory() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Double Letter";
    }

    @Override
    public String toString() {
        return "Double Letter: " + this.letter.toUpperCase();
    }
}
