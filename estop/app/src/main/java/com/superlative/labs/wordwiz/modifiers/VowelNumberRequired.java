package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by Catherine on 11/25/2015.
 */
public class VowelNumberRequired extends BaseChallengeModifier {

    private String letter;

    public VowelNumberRequired(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        return this.countVowels(word) == Integer.parseInt(letter);
    }

    @Override
    public boolean isMandatory() {
        return true;
    }

    @Override
    public String getDescription() {
        return null;
    }

    private int countVowels(String word)
    {
        int i, length, vowels = 0;
        String j;
        length = word.length();
        for (i = 0; i < length; i++)
        {

            j = word.substring(i, i + 1);
            System.out.println(j);

            if (j.equalsIgnoreCase("a") == true)
                vowels++;
            else if (j.equalsIgnoreCase("e") == true)
                vowels++;
            else if (j.equalsIgnoreCase("i") == true)
                vowels++;
            else if (j.equalsIgnoreCase("o") == true)
                vowels++;
            else if (j.equalsIgnoreCase("u") == true)
                vowels++;

        }
        return vowels;
    }

    @Override
    public String toString() {
        return "Vowels: " + this.letter;
    }
}
