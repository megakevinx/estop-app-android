package com.superlative.labs.wordwiz.modifiers;

/**
 * Created by kevin on 8/18/2015.
 */
public class LetterUseBonus extends BaseChallengeModifier {

    private String letter;

    public LetterUseBonus(int id, String letter) {
        this.id = id;
        this.letter = letter;
    }

    @Override
    public int getPointsValue() {
        return 100;
    }

    @Override
    public boolean meetsRequirement(String word) {
        return word.toLowerCase().contains(this.letter.toLowerCase());
    }

    @Override
    public boolean isMandatory() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Letter Use Bonus";
    }

    @Override
    public String toString() {
        return "Use: " + this.letter.toUpperCase();
    }
}
