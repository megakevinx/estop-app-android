package com.superlative.labs.wordwiz.data.repositories;

import android.util.Log;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Level;

/**
 * Created by kevin on 9/16/2015.
 */
public class LevelRepository implements ILevelRepository {

    private RuntimeExceptionDao<Level, Integer> dao;

    public LevelRepository(DatabaseHelper helper) {
        this.dao = helper.getLevelDao();
    }

    public Level getById(int levelId) {
        return this.dao.queryForId(levelId);
    }

    public void markLevelAsCompleted(int levelId, int totalScore) {

        UpdateBuilder<Level, Integer> updateBuilder = this.dao.updateBuilder();

        try {
            // set the criteria like you would a QueryBuilder
            updateBuilder.where().eq(Level.ID, levelId);

            // update the value of your field(s)
            updateBuilder.updateColumnValue(Level.SCORE, totalScore);
            updateBuilder.updateColumnValue(Level.IS_COMPLETED, true);

            updateBuilder.update();
        }
        catch (SQLException ex) {
            Log.e("LevelRepository", ex.getMessage());
        }
    }

    public List<Level> getByStageId(int stageId) {

        List<Level> levels = this.dao.queryForEq(Level.STAGE_ID, stageId);

        return levels;
    }

    public void setLevelScore(int levelId, int totalScore) {

        UpdateBuilder<Level, Integer> updateBuilder = this.dao.updateBuilder();

        try {
            // set the criteria like you would a QueryBuilder
            updateBuilder.where().eq(Level.ID, levelId);

            // update the value of your field(s)
            updateBuilder.updateColumnValue(Level.SCORE, totalScore);
            updateBuilder.update();
        }
        catch (SQLException ex) {
            Log.e("LevelRepository", ex.getMessage());
        }
    }

    public int getTotalScore() {

        try {
            GenericRawResults<Integer> rawResults =
                this.dao.queryRaw(
                    "SELECT SUM(score) FROM level",
                    new RawRowMapper<Integer>() {
                        public Integer mapRow(String[] columnNames, String[] resultColumns) {
                            return Integer.parseInt(resultColumns[0]);
                        }
                    });

            List<Integer> results = rawResults.getResults();

            return results.get(0);
        }
        catch (SQLException se) {
            Log.e("LevelRepository", se.getMessage());
        }
        catch (Exception e) {
            Log.e("LevelRepository", e.getMessage());
        }

        return 0;
    }

    public int getCompletedLevelsCount() {

        try {
            QueryBuilder<Level, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq(Level.IS_COMPLETED, true);
            queryBuilder.setCountOf(true);

            int completedLevels = (int) this.dao.countOf(queryBuilder.prepare());

            return completedLevels;
        }
        catch (RuntimeException re) {
            Log.e("LevelRepository", re.getMessage());
        }
        catch (SQLException se) {
            Log.e("LevelRepository", se.getMessage());
        }

        return 0;
    }

    public Level getNextLevel(int currentLevelId) {
        return this.dao.queryForId(currentLevelId + 1);
    }

    public void unlockNextLevel(Level level) {

        UpdateBuilder<Level, Integer> updateBuilder = this.dao.updateBuilder();

        try {
            // set the criteria like you would a QueryBuilder
            updateBuilder.where().eq(Level.ID, level.getId() + 1);

            // update the value of your field(s)
            updateBuilder.updateColumnValue(Level.IS_LOCKED, false);

            updateBuilder.update();
        }
        catch (SQLException ex) {
            Log.e("LevelRepository", ex.getMessage());
        }
    }
}
