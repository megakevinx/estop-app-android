package com.superlative.labs.wordwiz.data.repositories;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Category;
import com.superlative.labs.wordwiz.data.model.Theme;

/**
 * Created by Catherine on 7/18/2015.
 */
public class CategoryRepository {

    private RuntimeExceptionDao<Category, Integer> dao;

    public CategoryRepository(DatabaseHelper helper) {
        this.dao = helper.getCategoryDao();
    }

    public List<Category> getCategoriesByTheme(Theme theme){
        List<Category> categories = null;

        categories = this.dao.queryForEq("theme_id", theme.getId());

        return categories;
    }
}
