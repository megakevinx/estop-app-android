package com.superlative.labs.wordwiz.data.repositories;

import android.util.Log;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.PowerUp;
import com.superlative.labs.wordwiz.engine.Keys;

/**
 * Created by kevin on 11/7/2015.
 */
public class PowerUpRepository {

    private RuntimeExceptionDao<PowerUp, Integer> dao;

    public PowerUpRepository(DatabaseHelper helper) {
        this.dao = helper.getPowerUpDao();
    }

    public void addPowerUp(String sku, int quantity) {

        try {
            QueryBuilder<PowerUp, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq(PowerUp.SKU, sku);

            List<PowerUp> matchingProducts = this.dao.query(queryBuilder.prepare());

            if (matchingProducts != null && matchingProducts.size() > 0) {

                int updatedQuantity = matchingProducts.get(0).getQuantity() + quantity;

                UpdateBuilder<PowerUp, Integer> updateBuilder = this.dao.updateBuilder();
                updateBuilder.where().eq(PowerUp.SKU, sku);
                updateBuilder.updateColumnValue(PowerUp.QUANTITY, updatedQuantity);

                updateBuilder.update();
            }
            else {
                this.dao.create(new PowerUp(sku, quantity));
            }
        }
        catch (SQLException ex) {
            Log.e("PowerUpRepository", ex.getMessage());
        }
    }

    public void decreasePowerUpQuantity(String sku, int quantityToDecrease) {

        try {
            QueryBuilder<PowerUp, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq(PowerUp.SKU, sku);

            List<PowerUp> matchingProducts = this.dao.query(queryBuilder.prepare());

            if (matchingProducts != null && matchingProducts.size() > 0) {

                int updatedQuantity = matchingProducts.get(0).getQuantity() - quantityToDecrease;

                if (updatedQuantity < 1) updatedQuantity = 0; // Let's make sure we don't end up with a negative quantity.

                UpdateBuilder<PowerUp, Integer> updateBuilder = this.dao.updateBuilder();
                updateBuilder.where().eq(PowerUp.SKU, sku);
                updateBuilder.updateColumnValue(PowerUp.QUANTITY, updatedQuantity);

                updateBuilder.update();
            }
            else {
                Log.e("PowerUpRepository", "Product not purchased");
            }
        }
        catch (SQLException ex) {
            Log.e("PowerUpRepository", ex.getMessage());
        }
    }

    public int getCurrentLives() {

        int numberOfLives = 0;

        try {
            QueryBuilder<PowerUp, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq(PowerUp.SKU, Keys.Products.Skus.Life);

            PowerUp lives = this.dao.queryForFirst(queryBuilder.prepare());

            if (lives != null) {
                numberOfLives = lives.getQuantity();
            }
        }
        catch (SQLException ex) {
            Log.e("PowerUpRepository", ex.getMessage());
        }

        return numberOfLives;
    }

    public int getCurrentTimePlus() {

        int numberOfTimePlusItems = 0;

        try {
            QueryBuilder<PowerUp, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq(PowerUp.SKU, Keys.Products.Skus.TimePlus);

            PowerUp timePlusItems = this.dao.queryForFirst(queryBuilder.prepare());

            if (timePlusItems != null) {
                numberOfTimePlusItems = timePlusItems.getQuantity();
            }
        }
        catch (SQLException ex) {
            Log.e("PowerUpRepository", ex.getMessage());
        }

        return numberOfTimePlusItems;
    }

    public boolean getHasRemovedAds() {

        boolean hasRemovedAds = false;

        try {
            QueryBuilder<PowerUp, Integer> queryBuilder = this.dao.queryBuilder();
            queryBuilder.where().eq(PowerUp.SKU, Keys.Products.Skus.RemoveAds);

            PowerUp removeAdsItem = this.dao.queryForFirst(queryBuilder.prepare());

            if (removeAdsItem != null) {
                if (removeAdsItem.getQuantity() >= 1) {
                    hasRemovedAds = true;
                }
            }
        }
        catch (SQLException ex) {
            Log.e("PowerUpRepository", ex.getMessage());
        }

        return hasRemovedAds;
    }
}
