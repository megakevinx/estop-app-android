package com.superlative.labs.wordwiz;

import android.os.Bundle;

import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class CreditsActivity extends BaseGameActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }
}
