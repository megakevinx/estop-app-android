package com.superlative.labs.wordwiz.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.IOException;
import java.sql.SQLException;

import com.superlative.labs.wordwiz.data.loaders.DataLoader;
import com.superlative.labs.wordwiz.data.model.Category;
import com.superlative.labs.wordwiz.data.model.Level;
import com.superlative.labs.wordwiz.data.model.LevelChallenge;
import com.superlative.labs.wordwiz.data.model.LevelChallengeModifier;
import com.superlative.labs.wordwiz.data.model.PowerUp;
import com.superlative.labs.wordwiz.data.model.Stage;
import com.superlative.labs.wordwiz.data.model.Theme;
import com.superlative.labs.wordwiz.data.model.Word;

/**
 * Created by Catherine on 6/23/2015.
 */
public class RealDatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "estop.db";
    private static final int DATABASE_VERSION = 2;

    private static final String THEME_DATA_FILE = "theme.data.csv";
    private static final String CATEGORY_DATA_FILE = "category.data.csv";
    private static final String WORD_DATA_FILE = "word.data.csv";

    private static final String STAGE_DATA_FILE = "stage.data.csv";
    private static final String LEVEL_DATA_FILE = "level.data.csv";
    private static final String LEVEL_CHALLENGE_DATA_FILE = "level_challenge.data.csv";
    private static final String LEVEL_CHALLENGE_MODIFIER_DATA_FILE = "level_challenge_modifier.data.csv";

    private Context context;

    private RuntimeExceptionDao<Theme, Integer> themeRuntimeDao;
    private RuntimeExceptionDao<Category, Integer> categoryRuntimeDao;
    private RuntimeExceptionDao<Word, Integer> wordRuntimeDao;

    private RuntimeExceptionDao<Stage, Integer> stageRuntimeDao;
    private RuntimeExceptionDao<Level, Integer> levelRuntimeDao;
    private RuntimeExceptionDao<LevelChallenge, Integer> levelChallengeRuntimeDao;
    private RuntimeExceptionDao<LevelChallengeModifier, Integer> levelChallengeModifierRuntimeDao;

    private RuntimeExceptionDao<PowerUp, Integer> powerUpRuntimeDao;

    public RealDatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION
                //, R.raw.ormlite_config
                );

        this.context = context;
    }

    public Context getContext() {
        return this.context;
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();

        categoryRuntimeDao = null;
        wordRuntimeDao = null;
        themeRuntimeDao = null;

        stageRuntimeDao = null;
        levelRuntimeDao = null;
        levelChallengeRuntimeDao = null;
        levelChallengeModifierRuntimeDao = null;

        powerUpRuntimeDao = null;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(RealDatabaseHelper.class.getName(), "onCreate");

            TableUtils.createTable(connectionSource, Theme.class);
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, Word.class);

            TableUtils.createTable(connectionSource, Stage.class);
            TableUtils.createTable(connectionSource, Level.class);
            TableUtils.createTable(connectionSource, LevelChallenge.class);
            TableUtils.createTable(connectionSource, LevelChallengeModifier.class);

            TableUtils.createTable(connectionSource, PowerUp.class);
        }
        catch (SQLException e){
            Log.e(RealDatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException();
        }

        DataLoader dataLoader = new DataLoader(this);

        try {
            dataLoader.loadThemeData(RealDatabaseHelper.THEME_DATA_FILE);
            dataLoader.loadCategoryData(RealDatabaseHelper.CATEGORY_DATA_FILE);
            dataLoader.loadWordData(RealDatabaseHelper.WORD_DATA_FILE);

            dataLoader.loadStageData(RealDatabaseHelper.STAGE_DATA_FILE);
            dataLoader.loadLevelData(RealDatabaseHelper.LEVEL_DATA_FILE);
            dataLoader.loadLevelChallengeData(RealDatabaseHelper.LEVEL_CHALLENGE_DATA_FILE);
            dataLoader.loadLevelChallengeModifierData(RealDatabaseHelper.LEVEL_CHALLENGE_MODIFIER_DATA_FILE);

            //dataLoader.loadPowerUpData(); There's nothing to load for this table.
        }
        catch (IOException ioex) {
            Log.i(RealDatabaseHelper.class.getName(), "Couldn't load the stages");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        // nothing for now...

        Log.i(RealDatabaseHelper.class.getName(), "onUpgrade");
    }

    public RuntimeExceptionDao<Theme, Integer> getThemeDao() {
        if (themeRuntimeDao == null) {
            themeRuntimeDao = getRuntimeExceptionDao(Theme.class);
        }
        return themeRuntimeDao;
    }

    public RuntimeExceptionDao<Category, Integer> getCategoryDao() {
        if (categoryRuntimeDao == null) {
            categoryRuntimeDao = getRuntimeExceptionDao(Category.class);
        }
        return categoryRuntimeDao;
    }

    public RuntimeExceptionDao<Word, Integer> getWordDao() {
        if (wordRuntimeDao == null) {
            wordRuntimeDao = getRuntimeExceptionDao(Word.class);
        }
        return wordRuntimeDao;
    }

    public RuntimeExceptionDao<Stage, Integer> getStageDao() {
        if (stageRuntimeDao == null) {
            stageRuntimeDao = getRuntimeExceptionDao(Stage.class);
        }
        return stageRuntimeDao;
    }

    public RuntimeExceptionDao<Level, Integer> getLevelDao() {
        if (levelRuntimeDao == null) {
            levelRuntimeDao = getRuntimeExceptionDao(Level.class);
        }
        return levelRuntimeDao;
    }

    public RuntimeExceptionDao<LevelChallenge, Integer> getLevelChallengeDao() {
        if (levelChallengeRuntimeDao == null) {
            levelChallengeRuntimeDao = getRuntimeExceptionDao(LevelChallenge.class);
        }
        return levelChallengeRuntimeDao;
    }

    public RuntimeExceptionDao<LevelChallengeModifier, Integer> getLevelChallengeModifierDao() {
        if (levelChallengeModifierRuntimeDao == null) {
            levelChallengeModifierRuntimeDao = getRuntimeExceptionDao(LevelChallengeModifier.class);
        }
        return levelChallengeModifierRuntimeDao;
    }

    public RuntimeExceptionDao<PowerUp, Integer> getPowerUpDao() {
        if (powerUpRuntimeDao == null) {
            powerUpRuntimeDao = getRuntimeExceptionDao(PowerUp.class);
        }
        return powerUpRuntimeDao;
    }
}
