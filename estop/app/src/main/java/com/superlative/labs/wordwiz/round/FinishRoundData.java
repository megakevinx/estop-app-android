package com.superlative.labs.wordwiz.round;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedHashMap;
import java.util.Map;


public class FinishRoundData implements Parcelable {

    int secondsLeft;
    int levelId;
    LinkedHashMap<Integer, String> playedWordsForChallengeId;

    public FinishRoundData(int levelId, LinkedHashMap<Integer, String> playedWordsForChallengeId, int secondsLeft) {

        this.levelId = levelId;
        this.playedWordsForChallengeId = playedWordsForChallengeId;
        this.secondsLeft = secondsLeft;
    }

    public int getSecondsLeft() {
        return this.secondsLeft;
    }

    public int getLevelId() {
        return this.levelId;
    }

    public LinkedHashMap<Integer, String> getPlayedWordsForChallengeId() {
        return this.playedWordsForChallengeId;
    }

    // 99.9% of the time you can just ignore this
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    public void writeToParcel(Parcel out, int flags) {

        out.writeInt(this.levelId);
        out.writeInt(this.secondsLeft);

        int[] challengeIdsToParcel = new int[this.playedWordsForChallengeId.size()];
        String[] wordsToParcel = new String[this.playedWordsForChallengeId.size()];

        int index = 0;

        for(Map.Entry<Integer, String> entry : this.playedWordsForChallengeId.entrySet()) {
            Integer challengeId = entry.getKey();
            String word = entry.getValue();

            challengeIdsToParcel[index] = challengeId;
            wordsToParcel[index] = word;

            index++;
        }

        out.writeIntArray(challengeIdsToParcel);
        out.writeStringArray(wordsToParcel);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<FinishRoundData> CREATOR = new Parcelable.Creator<FinishRoundData>() {
        public FinishRoundData createFromParcel(Parcel in) {
            return new FinishRoundData(in);
        }

        public FinishRoundData[] newArray(int size) {
            return new FinishRoundData[size];
        }
    };

    // constructor that takes a Parcel and gives you an object populated with it's values
    private FinishRoundData(Parcel in) {

        this.playedWordsForChallengeId = new LinkedHashMap<>();

        this.levelId = in.readInt();
        this.secondsLeft = in.readInt();

        int[] challengeIdsToLoad = in.createIntArray();
        String[] wordsToLoad = in.createStringArray();

        for (int i = 0; i < challengeIdsToLoad.length; i++) {
            this.playedWordsForChallengeId.put(challengeIdsToLoad[i], wordsToLoad[i]);
        }
    }
}
