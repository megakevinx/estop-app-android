package com.superlative.labs.wordwiz.data.repositories;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.LevelChallenge;

/**
 * Created by kevin on 9/19/2015.
 */
public class LevelChallengeRepository {

    private final String LOG_TAG = getClass().getSimpleName();

    private RuntimeExceptionDao<LevelChallenge, Integer> dao;

    public LevelChallengeRepository(DatabaseHelper helper) {
        this.dao = helper.getLevelChallengeDao();
    }

    public LevelChallenge getById(int levelChallengeId) {
        return this.dao.queryForId(levelChallengeId);
    }
}
