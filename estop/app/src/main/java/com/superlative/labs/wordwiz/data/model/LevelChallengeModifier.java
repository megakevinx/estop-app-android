package com.superlative.labs.wordwiz.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by kevin on 8/29/2015.
 */

@DatabaseTable
public class LevelChallengeModifier {
    @DatabaseField(id = true)
    int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    LevelChallenge levelChallenge;

    @DatabaseField
    Modifier modifier;

    @DatabaseField
    String letter;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    Category category;

    @DatabaseField
    int value;

    LevelChallengeModifier() { }

    public LevelChallengeModifier(int id, LevelChallenge levelChallenge, Modifier modifier,
                                  String letter, Category category, int value) {
        this.id = id;
        this.levelChallenge = levelChallenge;
        this.modifier = modifier;
        this.letter = letter;
        this.category = category;
        this.value = value;

    }

    public int getId() {
        return id;
    }

    public LevelChallenge getLevelChallenge() {
        return levelChallenge;
    }

    public Modifier getModifier() {
        return modifier;
    }

    public String getLetter() {
        return letter;
    }

    public Category getCategory() {
        return category;
    }

    public int getValue() {
        return value;
    }
}
