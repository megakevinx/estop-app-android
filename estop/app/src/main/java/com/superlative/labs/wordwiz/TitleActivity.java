package com.superlative.labs.wordwiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.games.Games;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.repositories.LevelRepository;
import com.superlative.labs.wordwiz.data.repositories.PowerUpRepository;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.utils.AppRater;
import com.superlative.labs.wordwiz.utils.Constants;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;


public class TitleActivity extends BaseGameActivity {

    private static final int RC_UNUSED = 5001;
    private static final int ACHIEVEMENTS_REQUEST_CODE = 1000;
    private final String LOG_TAG = getClass().getSimpleName();
    InterstitialAd mInterstitialAd;
    Intent serviceIntent;

    private LevelRepository levelRepository;
    private PowerUpRepository powerUpRepository;

    private boolean mIsBound = false;
    Animation scaleButtonAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);

        //this.powerUpRepository.addPowerUp(Keys.Products.Skus.RemoveAds, Keys.Products.PackQuantity.One);

        this.initialize();
        this.displayAd();

        this.grantLives();

        //this.powerUpRepository.decreasePowerUpQuantity(Keys.Products.Skus.Life, 100);
        //this.powerUpRepository.addPowerUp(Keys.Products.Skus.Life, 100);

        //this.powerUpRepository.addPowerUp(Keys.Products.Skus.TimePlus, Keys.Products.PackQuantity.Five);

        this.displayProgress();

        AppRater.app_launched(this);
    }

    private void initialize() {
        DatabaseHelper databaseHelper = new DatabaseHelper(this);

        try {
            databaseHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }

        try {
            databaseHelper.openDataBase();
        }catch(SQLException sqle){

            throw new Error("Unable to open database");

        }

        this.levelRepository = new LevelRepository(databaseHelper);
        this.powerUpRepository = new PowerUpRepository(databaseHelper);

        this.scaleButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down);
    }

    private void displayAd() {

        boolean hasRemovedAds = this.powerUpRepository.getHasRemovedAds();

        if (!hasRemovedAds) {
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(Constants.DEVICE_ID)
                    .build();
            mAdView.loadAd(adRequest);
        }
    }

    /* Game Helper related stuff */
    @Override
    public void onSignInFailed() {
        Log.i(LOG_TAG, "Sign in failed.");
    }

    @Override
    public void onSignInSucceeded() {
        Log.i(LOG_TAG, "Sign in succeeded.");
    }

    private void grantLives() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor preferencesEditor = preferences.edit();

        boolean firstTimeLivesGranted = preferences.getBoolean(Keys.Preferences.FirstTimeLivesGranted, false);

        if (!firstTimeLivesGranted) {
            this.powerUpRepository.addPowerUp(Keys.Products.Skus.Life, Keys.Products.PackQuantity.Fifty);

            preferencesEditor.putBoolean(Keys.Preferences.FirstTimeLivesGranted, true); // value to store
            preferencesEditor.apply();
        }

        long millisecondsLastTimeLivesGranted = preferences.getLong(Keys.Preferences.LastTimeLivesGranted, 0);
        Date lastTimeLivesGranted = new Date(millisecondsLastTimeLivesGranted);

        Date now = new Date();

        if (hoursDifference(now, lastTimeLivesGranted) >= Keys.Settings.HoursToGrantLives) {

            this.powerUpRepository.addPowerUp(Keys.Products.Skus.Life, Keys.Products.PackQuantity.Five);

            preferencesEditor.putLong(Keys.Preferences.LastTimeLivesGranted, now.getTime()); // value to store
            preferencesEditor.apply();
        }

        if (this.powerUpRepository.getCurrentLives() == 0) {
            this.startGetLivesActivity();
        }
    }

    private static int hoursDifference(Date date1, Date date2) {
        final long MILLI_TO_HOUR = 1000 * 60 * 60;
        return (int) ((date1.getTime() - date2.getTime()) / MILLI_TO_HOUR);
    }

    private void displayProgress() {

        TextView textViewTotalScore = (TextView) this.findViewById(R.id.textView_TotalScore);
        TextView textViewCompletedLevels = (TextView) this.findViewById(R.id.textView_CompletedLevels);
        TextView textViewCurrentStage = (TextView) this.findViewById(R.id.textView_Lives);

        textViewTotalScore.setText(Integer.toString(this.levelRepository.getTotalScore()));
        textViewCompletedLevels.setText(Integer.toString(this.levelRepository.getCompletedLevelsCount()));
        textViewCurrentStage.setText(Integer.toString(this.powerUpRepository.getCurrentLives()));
    }


    public void button_SinglePlayer_OnClick(View sender) {

        Animation buttonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down);

        buttonAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                startStageSelectorActivity();
            }
        });

        sender.startAnimation(buttonAnimation);
    }

    public void button_leaderboard_OnClick(View sender) {

        Animation buttonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down);

        buttonAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                showLeaderboard();
            }
        });

        sender.startAnimation(buttonAnimation);
    }

    public void button_Store_OnClick(View sender) {

        Animation buttonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down);

        buttonAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                startStoreActivity();
            }
        });

        sender.startAnimation(buttonAnimation);
    }

    public void button_Achievements_OnClick(View sender) {

        Animation buttonAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_and_down);

        buttonAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                startActivityForResult(
                        Games.Achievements.getAchievementsIntent(getApiClient()),
                        ACHIEVEMENTS_REQUEST_CODE);
            }
        });

        sender.startAnimation(buttonAnimation);
    }

    public void button_Tutorial_OnClick(View sender){
        startTutorialActivity();
    }

    private void startTutorialActivity() {
        Intent intent = new Intent(this, TutorialActivity.class);
        this.startActivity(intent);
    }

    private void startStageSelectorActivity() {
        Intent intent = new Intent(this, StageSelectorActivity.class);
        this.startActivity(intent);
    }

    private void startStoreActivity() {
        Intent intent = new Intent(this, StoreActivity.class);
        this.startActivity(intent);
    }

    private void startGetLivesActivity() {
        Intent intent = new Intent(this, GetLivesActivity.class);
        this.startActivity(intent);
    }

    private void showToast(String message) {
        Context context = getApplicationContext();
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private void showLeaderboard() {
        if(isSignedIn()){
            startActivityForResult(
                    Games.Leaderboards.getLeaderboardIntent(this.getApiClient(), Keys.LEADERBOARD_ID),
                    RC_UNUSED);
        }
        else {
            Log.i(LOG_TAG, "NOT SIGNED IN");
        }
    }
}
