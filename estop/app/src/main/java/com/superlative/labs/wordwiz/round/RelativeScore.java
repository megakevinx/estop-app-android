package com.superlative.labs.wordwiz.round;

public enum RelativeScore 
{
	Full, Half, None
}