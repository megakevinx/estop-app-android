package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.utils.ApplicationData;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class GetTimePlusActivity extends BaseGameActivity {

    boolean hasTimePlus = false;

    String purchaseMessage = "No biggie! Head up to the store to get some extra seconds!";
    String useMessage = "No biggie! Use one of your Time Plus PowerUps!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_time_plus);

        Intent intent = this.getIntent();

        this.hasTimePlus = intent.getBooleanExtra(Keys.IntentParameters.HasTimePlus, false);

        TextView textViewMessage = (TextView) this.findViewById(R.id.textView_Message);

        if (this.hasTimePlus)
        {
            textViewMessage.setText(useMessage);
        }
        else
        {
            textViewMessage.setText(purchaseMessage);
        }
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    public void button_PurchaseTimePlusPack_OnClick(View sender) {

        ApplicationData appData = (ApplicationData) getApplication();
        appData.setUseTimePlusPowerUp(true);

        if (!this.hasTimePlus) {
            this.startStoreActivity();
        }
        else
        {
            this.finish();
        }
    }

    public void button_GoToScore_OnClick(View sender){
        finish();
    }

    private void startStoreActivity() {
        Intent intent = new Intent(this, StoreActivity.class);

        intent.putExtra(Keys.IntentParameters.AutoBuy, true);
        intent.putExtra(Keys.IntentParameters.AutoBuySku, Keys.Products.Skus.TimePlus);

        this.startActivity(intent);
    }
}
