package com.superlative.labs.wordwiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.superlative.labs.wordwiz.data.DatabaseHelper;
import com.superlative.labs.wordwiz.data.model.Level;
import com.superlative.labs.wordwiz.data.repositories.LevelChallengeRepository;
import com.superlative.labs.wordwiz.data.repositories.LevelRepository;
import com.superlative.labs.wordwiz.data.repositories.PowerUpRepository;
import com.superlative.labs.wordwiz.data.repositories.StageRepository;
import com.superlative.labs.wordwiz.data.repositories.WordRepository;
import com.superlative.labs.wordwiz.engine.Keys;
import com.superlative.labs.wordwiz.gui.store.InAppProductsListAdapter;
import com.superlative.labs.wordwiz.utils.billing.IabHelper;
import com.superlative.labs.wordwiz.utils.billing.IabResult;
import com.superlative.labs.wordwiz.utils.billing.Inventory;
import com.superlative.labs.wordwiz.utils.billing.Purchase;
import com.superlative.labs.wordwiz.utils.billing.SkuDetails;
import com.superlative.labs.wordwiz.utils.googleplay.BaseGameActivity;

public class StoreActivity extends BaseGameActivity {

    private String TAG = "StoreActivity";

    private String connectionErrorMessage = "Could not connect to the store. Please try again later.";

    private IabHelper iabHelper;

    //private String SKU_TEST = "android.test.purchased";

    private List<String> productSkus;
    private List<SkuDetails> productsForSale;
    private PowerUpRepository powerUpRepository;
    private LevelRepository levelRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        this.initialize();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (this.iabHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!this.iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initialize() {

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        this.powerUpRepository = new PowerUpRepository(databaseHelper);
        this.levelRepository = new LevelRepository(databaseHelper);

        this.productSkus = Arrays.asList(Keys.Products.Skus.TimePlus, Keys.Products.Skus.Life, Keys.Products.Skus.RemoveAds);
        this.productsForSale = new ArrayList<>();

        this.iabHelper = new IabHelper(this, Keys.PublicKey);

        this.iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                    showToast(connectionErrorMessage);
                }

                loadProducts();
            }
        });
    }

    private void loadProducts() {

        this.iabHelper.queryInventoryAsync(true, this.productSkus, new IabHelper.QueryInventoryFinishedListener() {

            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

                if (result.isFailure()) {
                    // handle error
                    Log.d(TAG, "error while querying the inventory");
                    showToast(connectionErrorMessage);
                    return;
                }

                for (String sku : productSkus) {

                    SkuDetails product = inventory.getSkuDetails(sku);

                    if (product != null) {
                        productsForSale.add(product);
                    } else {
                        Log.d(TAG, "product not found: " + sku);
                    }
                }

                displayProducts();
            }
        });
    }

    private void displayProducts() {

        ListView productsListView = (ListView)findViewById(R.id.listView_products);

        final InAppProductsListAdapter productsAdapter = new InAppProductsListAdapter(this, this.productsForSale);

        productsListView.setAdapter(productsAdapter);

        productsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SkuDetails productToPurchase = (SkuDetails) productsAdapter.getItem(position);

                Log.d(TAG, "onItemClick sku: " + productToPurchase.getSku());
                Log.d(TAG, "purchasing sku: " + productToPurchase.getSku());

                purchaseProduct(productToPurchase);
            }
        });

        this.checkAutoBuy();
    }

    private void checkAutoBuy() {

        Intent intent = getIntent();

        boolean autoBuy = intent.getBooleanExtra(Keys.IntentParameters.AutoBuy, false);
        String autoBuySku = intent.getStringExtra(Keys.IntentParameters.AutoBuySku);

        //this.showToast(Boolean.toString(autoBuy) + " - " + autoBuySku);

        if (autoBuy) {
            purchaseProduct(autoBuySku);
        }
    }

    public void purchaseProduct(SkuDetails productToPurchase) {
        this.purchaseProduct(productToPurchase.getSku());
    }

    private void purchaseProduct(final String productSkuToPurchase) {
        this.iabHelper.launchPurchaseFlow(
                this,
                productSkuToPurchase,
                10001,
                new IabHelper.OnIabPurchaseFinishedListener() {
                    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                        if (result.isFailure()) {
                            Log.d(TAG, "Error purchasing: " + result);
                            showToast(connectionErrorMessage);
                            return;
                        } else if (purchase.getSku().equals(productSkuToPurchase)) {
                            Log.d(TAG, "consuming sku: " + purchase.getSku());
                            consumeProduct(purchase);
                        }
                    }
                },
                "some random string shit");
    }

    private void consumeProduct(Purchase purchase) {

        this.iabHelper.consumeAsync(
                purchase,
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {
                            // provision the in-app purchase to the user
                            // (for example, credit 50 gold coins to player's character)
                            Log.d(TAG, "provisioning sku: " + purchase.getSku());
                            provisionProduct(purchase.getSku());
                        } else {
                            Log.d(TAG, "Error consuming: " + result);
                            showToast(connectionErrorMessage);
                        }
                    }
                });
    }

    private void provisionProduct(String sku) {

        //this.showToast("You have been provisioned with " + sku); // TODO: Remove

        if (sku.equals(Keys.Products.Skus.TimePlus)) {
            this.powerUpRepository.addPowerUp(Keys.Products.Skus.TimePlus, Keys.Products.PackQuantity.Twenty);
        }
        else if (sku.equals(Keys.Products.Skus.Life)) {
            this.powerUpRepository.addPowerUp(Keys.Products.Skus.Life, Keys.Products.PackQuantity.Ten);
        }
        else if (sku.equals(Keys.Products.Skus.RemoveAds)) {
            this.powerUpRepository.addPowerUp(Keys.Products.Skus.RemoveAds, Keys.Products.PackQuantity.One);
            this.showToast("Thanks! Adds will be gone after next app restart.");
        }
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

//    public void button_ConsumeTimePlus_OnClick(View sender) {
//        this.powerUpRepository.decreasePowerUpQuantity(Keys.Products.Skus.TimePlus, Keys.Products.UseQuantity.One);
//
//        this.logPowerUps(); // TODO: Remove
//    }
//
//    public void button_ConsumeHint_OnClick(View sender) {
//        this.powerUpRepository.decreasePowerUpQuantity(Keys.Products.Skus.Life, Keys.Products.UseQuantity.One);
//
//        this.logPowerUps(); // TODO: Remove
//    }

    // TODO: Remove this method, this is only for debugging.
//    private void logPowerUps() {
//
//        DatabaseHelper helper = new DatabaseHelper(this);
//        RuntimeExceptionDao<PowerUp, Integer> powerUpDao = helper.getPowerUpDao();
//
//        String text = "";
//
//        List<PowerUp> powerUps = powerUpDao.queryForAll();
//
//        for (PowerUp powerUp : powerUps) {
//
//            text += "PowerUps: " +
//                    powerUp.getId() + " - " +
//                    powerUp.getSku() + " - " +
//                    Integer.toString(powerUp.getQuantity()) +
//                    "\n";
//
//            Log.d(TAG,
//                    "PowerUps: " +
//                            powerUp.getId() + " - " +
//                            powerUp.getSku() + " - " +
//                            Integer.toString(powerUp.getQuantity()));
//        }
//
//        this.showToast(text);
//    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }
}
