This file contains the definition of all the *.data.csv files in the assets folder. Namely, what
field each column corresponds to.

theme.data.csv -> id,name,description
category.data.csv -> id,name,theme_id
word.data.csv -> id,name,category_id
stage.data.csv -> id,sequence,name
level.data.csv -> id,sequence,stage_id,name,seconds
level_challenge.data.csv -> id,level_id
level_challenge_modifier.data.csv -> id,level_challenge_id,modifier,letter,category_id,value
